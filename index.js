import 'react-native-gesture-handler';
import { registerRootComponent } from 'expo';
import App from './App';
import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { store, persistor } from './src/store/utilities/storeConfiguration';
import { PersistGate } from 'redux-persist/integration/react'

export default class WrappedApp extends Component {

    render() {
        return (
            <Provider store={store}>
                <PersistGate loading={null} persistor={persistor}>
                    <App/>
                </PersistGate>
            </Provider>
        );
    }
}

registerRootComponent(WrappedApp );

