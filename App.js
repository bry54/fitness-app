import React, { PureComponent} from 'react';
import {StyleSheet, StatusBar, View} from 'react-native';
import * as Font from 'expo-font';
import { Asset } from 'expo-asset';
import Colors from './src/constants/ColorDefinitions';
import 'react-native-gesture-handler'
import fonts from './src/assets/fonts';
import images from './src/assets/images';
import AppNavigator from "./src/navigators/AppNavigator";
import { AppLoading, SplashScreen } from 'expo';
import {updateGlobalControls} from "./src/store/actions/global";
import {connect} from "react-redux";

class App extends PureComponent {
  state = {
    fontsLoaded: false,
    imageLoaded: false,
  };

  async componentDidMount() {
    await this._localizeApp();
    await Font.loadAsync({
      'handwriting': fonts.greatVibesRegular,
      'alpha-regular': fonts.crimsonProRegular,
      'numeric-regular': fonts.b612MonoRegular
    }).then(res => this._updateState( 'fontsLoaded', true ))
        .catch(error => console.log(error))
  }

  render () {
    const splashReady = this.state.fontsLoaded && this.state.imageLoaded;
    if (!splashReady) {
      return (
          <AppLoading
              startAsync={this._cacheSplashResourcesAsync}
              onFinish={() => this._updateState( 'imageLoaded', true )}
              onError={console.warn}
              autoHideSplash={false}
          />
      )
    } else {
      SplashScreen.hide();
      return (
          <View style={styles.container}>
            <StatusBar barStyle="light-content"  backgroundColor={Colors.colorPrimaryDark}/>
            <AppNavigator/>
          </View>
      );
    }
  }

  _updateState = (inputKey, inputValue) => {
    this.setState({
      ...this.state,
      [inputKey] : inputValue
    });
  };

  _cacheSplashResourcesAsync = async () => {
    const splash = images.splash;
    return Asset.fromModule(splash).downloadAsync()
  };

  _localizeApp = () =>{
    const lang = this.props.language;
    this.props.updateGlobalControl({key: 'language', value: lang});
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: '100%',
    backgroundColor: Colors.mainBackground,
  },
});

const mapStateToProps = (state) => {
  return {
    language: state.globalReducer.language,
  }
};

const matchDispatchToProps = dispatch => {
  return {
    updateGlobalControl: (controlObj) => dispatch(updateGlobalControls(controlObj))
  }
};

export default connect(mapStateToProps, matchDispatchToProps)(App)
