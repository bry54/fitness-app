import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import LandingScreen from "../../screens/main/workouts/LandingScreen";
import ColorDefinitions from "../../constants/ColorDefinitions";
import MenuButton from "../../components/menu/MenuButton";
import SettingsButton from "../../components/menu/SettingsButton";
import ProgramDescriptionScreen from "../../screens/main/workouts/ProgramDescriptionScreen";
import ExerciseDescriptionScreen from "../../screens/main/exercises/ExerciseDescriptionScreen";
import ExerciseListScreen from "../../screens/main/workouts/ExerciseListScreen";
import i18n from "../../localizations/i18n";

const HomeStack = createStackNavigator();

export default () => (
    <HomeStack.Navigator
        initialRouteName="Dashboard"
        headerMode="screen"
        screenOptions={{
            headerTintColor: ColorDefinitions.primaryText.shade0,
            headerStyle: { backgroundColor: ColorDefinitions.colorPrimary },
        }}>

        <HomeStack.Screen
            name="Dashboard"
            component={LandingScreen}
            options={({ navigation, route }) => ({
                headerTitle: i18n.t('menu_workouts'),
                headerTitleStyle: {fontFamily: 'alpha-regular'},
                headerLeft: () => ( <MenuButton navigation={navigation}/> ),
                headerRight: () => ( <SettingsButton navigation={navigation}/> ),
            })}
        />

        <HomeStack.Screen
            name="ProgramDetail"
            component={ProgramDescriptionScreen}
            options={({ navigation, route }) => ({
                headerTitle: route.params.program.title + ' ' + i18n.t('lbl_workouts'),
                headerTitleStyle: {fontFamily: 'alpha-regular'},
                headerRight: () => ( <SettingsButton navigation={navigation}/> ),
            })}
        />

        <HomeStack.Screen
            name="Workout"
            component={ExerciseListScreen}
            options={({ navigation, route }) => ({
                headerTitle: route.params.program.dayName + ' ' + i18n.t('lbl_workouts'),
                headerTitleStyle: {fontFamily: 'alpha-regular'},
                headerRight: () => ( <SettingsButton navigation={navigation}/> ),
            })}
        />

        <HomeStack.Screen
            name="ExerciseDescription"
            component={ExerciseDescriptionScreen}
            options={({ navigation, route }) => ({
                headerTitle: route.params.exercise.title,
                headerTitleStyle: {fontFamily: 'alpha-regular'},
                headerRight: () => ( <SettingsButton navigation={navigation}/> ),
            })}
        />
    </HomeStack.Navigator>
)
