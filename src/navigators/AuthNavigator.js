import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import ColorDefinitions from "../constants/ColorDefinitions";
import RegistrationOptions from "../screens/auth/RegistrationOptions";
import SignInScreen from "../screens/auth/SignInScreen";
import SignUpScreen from "../screens/auth/SignUpScreen";
import ForgotPasswordScreen from "../screens/auth/ForgotPasswordScreen";
import i18n from "../localizations/i18n";

const AuthStack = createStackNavigator();

export default () => (
    <AuthStack.Navigator
        initialRouteName="CreateAccount"
        headerMode="screen"
        screenOptions={{
            headerTintColor: ColorDefinitions.colorAccent,
            headerStyle: { backgroundColor: ColorDefinitions.colorPrimary },
        }}>

        <AuthStack.Screen
            name="StartOptions"
            component={RegistrationOptions}
            options={({ navigation, route }) => ({
                title: null,
                headerShown: false
            })}
        />
        <AuthStack.Screen
            name="SignIn"
            component={SignInScreen}
            options={({ navigation, route }) => ({
                title: i18n.t('title_sign_in'),
                headerTitleStyle: {fontFamily: 'alpha-regular'},
            })}
        />
        <AuthStack.Screen
            name="SignUp"
            component={SignUpScreen}
            options={({ navigation, route }) => ({
                title: i18n.t('title_sign_up'),
                headerTitleStyle: {fontFamily: 'alpha-regular'},
            })}
        />
        <AuthStack.Screen
            name="ForgotPassword"
            component={ForgotPasswordScreen}
            options={({ navigation, route }) => ({
                title: i18n.t('title_forgot_password'),
                headerTitleStyle: {fontFamily: 'alpha-regular'},
            })}
        />
    </AuthStack.Navigator>
)
