import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {createMaterialTopTabNavigator} from "@react-navigation/material-top-tabs";
import LandingScreen from "../../screens/main/shop/LandingScreen";
import ColorDefinitions from "../../constants/ColorDefinitions";
import MenuButton from "../../components/menu/MenuButton";
import SettingsButton from "../../components/menu/SettingsButton";
import i18n from "../../localizations/i18n";
import ShopScreen from "../../screens/main/shop/ShopScreen";

const ShopStack = createStackNavigator();
const ShopTabsNavigator = createMaterialTopTabNavigator();

export default () => (
    <ShopStack.Navigator
        initialRouteName="Dashboard"
        headerMode="screen"
        screenOptions={{
            headerTintColor: ColorDefinitions.primaryText.shade0,
            headerStyle: { backgroundColor: ColorDefinitions.colorPrimary },
        }}>

        <ShopTabsNavigator.Screen
            name="TabsNavigator"
            component={TabsNavigator}
            options={({navigation, route}) => ({
                headerTitle: i18n.t('menu_shop'),
                headerTitleStyle: {fontFamily: 'alpha-regular'},
                headerLeft: () => (<MenuButton navigation={navigation}/>),
                headerRight: () => (<SettingsButton navigation={navigation}/>),
            })}
        />

    </ShopStack.Navigator>
)

const TabsNavigator = () => (
    <ShopTabsNavigator.Navigator
        lazy={true}
        initialRouteName="MembershipTab"
        headerMode="screen"
        tabBarOptions={{
            swipeEnabled: false,
            indicatorStyle: {borderColor: ColorDefinitions.secondaryBackground.shade0, borderBottomWidth: 2},
            labelStyle: {color: ColorDefinitions.primaryText.shade0, fontFamily: 'alpha-regular'},
            style: {backgroundColor: ColorDefinitions.colorPrimary},
        }}>

        <ShopTabsNavigator.Screen
            name="MembershipTab"
            component={LandingScreen}
            options={({navigation, route}) => ({
                title: i18n.t('tab_memberships'),
                headerShown: true
            })}
        />

        <ShopTabsNavigator.Screen
            name="ShoppingTab"
            component={ShopScreen}
            options={({navigation, route}) => ({
                title: i18n.t('tab_shop'),
                headerShown: true
            })}
        />
    </ShopTabsNavigator.Navigator>
);
