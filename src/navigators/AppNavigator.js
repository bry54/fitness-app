import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import SplashScreen from "../screens/SplashScreen";
import OnBoardingScreen from "../screens/OnBoardingScreen";
import AuthNavigator from "./AuthNavigator";
import MainDrawerNavigator from "./MainDrawerNavigator";

const AppStack = createStackNavigator();

export default () => (
    <NavigationContainer>
        <AppStack.Navigator headerMode='none'>
            <AppStack.Screen name="SplashScreen" component={SplashScreen} />
            <AppStack.Screen name="OnBoarding" component={OnBoardingScreen} />
            <AppStack.Screen name="Auth" component={AuthNavigator} />
            <AppStack.Screen name="Main" component={MainDrawerNavigator} />
        </AppStack.Navigator>
    </NavigationContainer>
)
