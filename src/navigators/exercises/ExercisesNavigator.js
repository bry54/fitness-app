import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import LandingScreen from "../../screens/main/exercises/LandingScreen";
import ExercisesScreen from "../../screens/main/exercises/ExercisesScreen";
import ColorDefinitions from "../../constants/ColorDefinitions";
import MenuButton from "../../components/menu/MenuButton";
import SettingsButton from "../../components/menu/SettingsButton";
import ExerciseDescriptionScreen from "../../screens/main/exercises/ExerciseDescriptionScreen";
import i18n from "../../localizations/i18n";

const HomeStack = createStackNavigator();

export default () => (
    <HomeStack.Navigator
        initialRouteName="Dashboard"
        headerMode="screen"
        screenOptions={{
            headerTintColor: ColorDefinitions.primaryText.shade0,
            headerStyle: { backgroundColor: ColorDefinitions.colorPrimary },
        }}>

        <HomeStack.Screen
            name="Dashboard"
            component={LandingScreen}
            options={({ navigation, route }) => ({
                headerTitle: i18n.t('menu_exercises'),
                headerTitleStyle: {fontFamily: 'alpha-regular'},
                headerLeft: () => ( <MenuButton navigation={navigation}/> ),
                headerRight: () => ( <SettingsButton navigation={navigation}/> ),
            })}
        />

        <HomeStack.Screen
            name="MuscleExercises"
            component={ExercisesScreen}
            options={({ navigation, route }) => ({
                headerTitle: route.params.muscle.title,
                headerTitleStyle: {fontFamily: 'alpha-regular'},
                headerRight: () => ( <SettingsButton navigation={navigation}/> ),
            })}
        />

        <HomeStack.Screen
            name="ExerciseDescription"
            component={ExerciseDescriptionScreen}
            options={({ navigation, route }) => ({
                headerTitle: route.params.exercise.title,
                headerTitleStyle: {fontFamily: 'alpha-regular'},
                headerRight: () => ( <SettingsButton navigation={navigation}/> ),
            })}
        />
    </HomeStack.Navigator>
)
