import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import LandingScreen from "../../screens/main/settings/LandingScreen";
import DummyScreen from "../../screens/main/home/LandingScreen";
import ColorDefinitions from "../../constants/ColorDefinitions";
import MenuButton from "../../components/menu/MenuButton";
import SettingsButton from "../../components/menu/SettingsButton";

const HomeStack = createStackNavigator();

export default () => (
    <HomeStack.Navigator
        initialRouteName="Dashboard"
        headerMode="screen"
        screenOptions={{
            headerTintColor: ColorDefinitions.primaryText.shade0,
            headerStyle: { backgroundColor: ColorDefinitions.colorPrimary },
        }}>

        <HomeStack.Screen
            name="Dashboard"
            component={LandingScreen}
            options={({ navigation, route }) => ({
                headerTitle: 'Settings',
                headerTitleStyle: {fontFamily: 'alpha-regular'},
                headerLeft: () => ( <MenuButton navigation={navigation}/> ),
                headerRight: () => ( <SettingsButton navigation={navigation}/> ),
            })}
        />

        <HomeStack.Screen
            name="Dummy"
            component={DummyScreen}
            options={({ navigation, route }) => ({
                headerTitle: 'Dummy Screen',
                headerTitleStyle: {fontFamily: 'alpha-regular'},
                headerRight: () => ( <SettingsButton navigation={navigation}/> ),
            })}
        />
    </HomeStack.Navigator>
)
