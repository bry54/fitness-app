import React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import NavigationDrawerContent from "../components/menu/NavigationDrawerContent";
import ColorDefinitions from "../constants/ColorDefinitions";
import HomeNavigator from "./home/HomeNavigator";
import WorkoutsNavigator from "./workouts/WorkoutsNavigator";
import ExercisesNavigator from "./exercises/ExercisesNavigator";
import DietsNavigator from "./diets/DietsNavigator";
import MotivationNavigator from "./motivation/MotivationNavigator";
import ProfileNavigator from "./profile/ProfileNavigator";
import LogsNavigator from "./logs/LogsNavigator";
import SettingsNavigator from "./settings/SettingsNavigator";
import ShopNavigator from "./shop/ShopNavigator";

const Drawer = createDrawerNavigator();

export default () => (
    <Drawer.Navigator
        initialRouteName='Home'
        drawerType='slide'
        drawerContent={props => <NavigationDrawerContent {...props} />}
        overlayColor={'rgba(1,222,1,.1)'}
        drawerContentOptions={{
            activeBackgroundColor: ColorDefinitions.colorPrimaryDark,
            activeTintColor: ColorDefinitions.primaryText.shade0,
            itemStyle: { marginVertical: 0 },
            labelStyle: { fontFamily: 'alpha-regular', fontWeight: '700', width: '100%'}
        }}
        drawerStyle={{
            backgroundColor: ColorDefinitions.colorPrimary,
        }}>
        <Drawer.Screen
            name="Home"
            component={HomeNavigator}
            options={({ navigation, route }) => ({

            })}
        />

        <Drawer.Screen
            name="Workouts"
            component={WorkoutsNavigator}
            options={({ navigation, route }) => ({

            })}
        />

        <Drawer.Screen
            name="Exercises"
            component={ExercisesNavigator}
            options={({ navigation, route }) => ({

            })}
        />

        <Drawer.Screen
            name="Diets"
            component={DietsNavigator}
            options={({ navigation, route }) => ({

            })}
        />

        <Drawer.Screen
            name="Motivation"
            component={MotivationNavigator}
            options={({ navigation, route }) => ({

            })}
        />

        <Drawer.Screen
            name="MyProfile"
            component={ProfileNavigator}
            options={({ navigation, route }) => ({

            })}
        />

        <Drawer.Screen
            name="GymShop"
            component={ShopNavigator}
            options={({ navigation, route }) => ({

            })}
        />

        <Drawer.Screen
            name="Logs"
            component={LogsNavigator}
            options={({ navigation, route }) => ({

            })}
        />

        <Drawer.Screen
            name="Settings"
            component={SettingsNavigator}
            options={({ navigation, route }) => ({

            })}
        />
    </Drawer.Navigator>
)

