import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import LandingScreen from "../../screens/main/diets/LandingScreen";
import DummyScreen from "../../screens/main/home/LandingScreen";
import ColorDefinitions from "../../constants/ColorDefinitions";
import MenuButton from "../../components/menu/MenuButton";
import SettingsButton from "../../components/menu/SettingsButton";
import MealDescriptionScreen from "../../screens/main/diets/MealDescriptionScreen";
import MealListingScreen from "../../screens/main/diets/MealListingScreen";
import i18n from "../../localizations/i18n";

const HomeStack = createStackNavigator();

export default () => (
    <HomeStack.Navigator
        initialRouteName="Dashboard"
        headerMode="screen"
        screenOptions={{
            headerTintColor: ColorDefinitions.primaryText.shade0,
            headerStyle: { backgroundColor: ColorDefinitions.colorPrimary },
        }}>

        <HomeStack.Screen
            name="Dashboard"
            component={LandingScreen}
            options={({ navigation, route }) => ({
                headerTitle: i18n.t('menu_diets'),
                headerTitleStyle: {fontFamily: 'alpha-regular'},
                headerLeft: () => ( <MenuButton navigation={navigation}/> ),
                headerRight: () => ( <SettingsButton navigation={navigation}/> ),
            })}
        />

        <HomeStack.Screen
            name="MealsListing"
            component={MealListingScreen}
            options={({ navigation, route }) => ({
                headerTitle: route.params.dietCategory.title_en,
                headerTitleStyle: {fontFamily: 'alpha-regular'},
                headerRight: () => ( <SettingsButton navigation={navigation}/> ),
            })}
        />

        <HomeStack.Screen
            name="MealDescription"
            component={MealDescriptionScreen}
            options={({ navigation, route }) => ({
                headerTitle: i18n.t('lbl_meal_description'),
                headerTitleStyle: {fontFamily: 'alpha-regular'},
                headerRight: () => ( <SettingsButton navigation={navigation}/> ),
            })}
        />
    </HomeStack.Navigator>
)
