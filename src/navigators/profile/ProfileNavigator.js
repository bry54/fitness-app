import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import LandingScreen from "../../screens/main/profile/LandingScreen";
import SettingsScreen from "../../screens/main/profile/SettingsScreen";
import ColorDefinitions from "../../constants/ColorDefinitions";
import MenuButton from "../../components/menu/MenuButton";
import SettingsButton from "../../components/menu/SettingsButton";
import MyGoalsScreen from "../../screens/main/profile/MyGoalsScreen";
import i18n from "../../localizations/i18n";

const ProfileNavigator = createStackNavigator();
const ProfileTabsNavigator = createMaterialTopTabNavigator();

export default () => (
    <ProfileNavigator.Navigator
        initialRouteName="TabsNavigator"
        headerMode={Platform.OS === 'ios' ? 'float' : 'screen'}
        screenOptions={{
            headerTintColor: ColorDefinitions.primaryText.shade0,
            headerStyle: {backgroundColor: ColorDefinitions.colorPrimary},
        }}>

        <ProfileNavigator.Screen
            name="TabsNavigator"
            component={TabsNavigator}

            options={({navigation, route}) => ({
                headerTitle: i18n.t('menu_profile'),
                headerTitleStyle: {fontFamily: 'alpha-regular'},
                headerLeft: () => (<MenuButton navigation={navigation}/>),
                headerRight: () => (<SettingsButton navigation={navigation}/>),
            })}
        />
    </ProfileNavigator.Navigator>
);

const TabsNavigator = () => (
    <ProfileTabsNavigator.Navigator
        lazy={true}
        initialRouteName="ProfileTab"
        headerMode="screen"
        tabBarOptions={{
            indicatorStyle: {borderColor: ColorDefinitions.secondaryBackground.shade0, borderBottomWidth: 2},
            labelStyle: {color: ColorDefinitions.primaryText.shade0, fontFamily: 'alpha-regular'},
            style: {backgroundColor: ColorDefinitions.colorPrimary},
        }}>

        <ProfileTabsNavigator.Screen
            name="ProfileTab"
            component={LandingScreen}
            options={({navigation, route}) => ({
                title: 'MY PROFILE',
                headerShown: true
            })}
        />

        <ProfileTabsNavigator.Screen
            name="GoalsTab"
            component={MyGoalsScreen}
            options={({navigation, route}) => ({
                title: 'GOALS',
                headerShown: true
            })}
        />
    </ProfileTabsNavigator.Navigator>
);
