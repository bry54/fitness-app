const desc = [
    { "gte": 0,    "lt": 15,   "name": "Very Severely Underweight" },
    { "gte": 15,   "lt": 16,   "name": "Severely Underweight" },
    { "gte": 16,   "lt": 18.5, "name": "Underweight" },
    { "gte": 18.5, "lt": 25,   "name": "Normal" },
    { "gte": 25,   "lt": 30,   "name": "Overweight" },
    { "gte": 30,   "lt": 35,   "name": "Moderately Obese" },
    { "gte": 35,   "lt": 40,   "name": "Severely Obese" },
    { "gte": 40,   "lt": 9999, "name": "Very Severely Obese" }
];

const fFatDesc = [
    {name: 'Essential Fat'  ,gte:10, lt: 13.9},
    {name: 'Athletic'       ,gte:14, lt: 20.9},
    {name: 'Fit'            ,gte:21, lt: 24.9},
    {name: 'Acceptable'     ,gte:25, lt: 31.9},
    {name: 'Obese'          ,gte:32, lt: 100000000},
];

const mFatDesc = [
    {name: 'Essential Fat'  ,gte: 2, lt: 5.9 },
    {name: 'Athletic'       ,gte: 6, lt: 13.9 },
    {name: 'Fit'            ,gte:14, lt: 17.9 },
    {name: 'Acceptable'     ,gte:18, lt: 24.9},
    {name: 'Obese'          ,gte:25, lt: 100000000},
]

const calculateFat = (gender, height, waist) => {
    const isMale = gender === 1;
    let ratio;
    if (isMale)
        ratio = 64-( 20 * (height/waist) )
    else
        ratio = 76-( 20 * (height/waist) )

    return ratio
}

const fatDescExtractor = (fatRatio, gender) =>{
    const list = gender === 1 ? mFatDesc : fFatDesc;
    const set =  list.find( (set) => {
        return (fatRatio >= set.gte && fatRatio <= set.lt)
    })
    return set && set.name
}

const calculator = (weight, height, imperial=false) => {
    const multiplier = imperial ? 703 : 1
    return (multiplier * weight) / (height * height)
};

const descExtractor = (bmi) =>{
    const set = desc.find( (set) => {
        return (bmi >= set.gte && bmi < set.lt)
    })
    return set && set.name
}

const bmiCalculator = (weight, height, imperial) =>{
    const bmi = calculator(weight, height/100, imperial)
    return {
        num: bmi.toFixed(2),
        desc: descExtractor(bmi)
    }
}

const fatRatioCalculator = (height, waist, gender) =>{
    const bodyFatRatio = calculateFat(gender, height, waist)
    return {
        num: bodyFatRatio.toFixed(2),
        desc: fatDescExtractor(bodyFatRatio, gender)
    }
}

export {bmiCalculator, fatRatioCalculator}
