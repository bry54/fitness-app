import images from "../../assets/images";

const muscles = [
    {
        id: 8,
        title: 'Abs',
        img: images.muscles.abs,
    },{
        id: 1,
        title: 'Biceps',
        img: images.muscles.biceps,

    },{
        id: 11,
        title: 'Cardio',
        img:images.muscles.cardio,

    },{
        id: 3,
        title: 'Chest',
        img:images.muscles.chest,

    },{
        id: 12,
        title: 'Core',
        img:images.muscles.core,

    },{
        id: 2,
        title: 'Legs',
        img:images.muscles.legs,

    },{
        id: 9,
        title: 'Shoulders',
        img: images.muscles.shoulder,

    },{
        id: 10,
        title: 'Back',
        img: images.muscles.back,

    },
]

const equipments = [
    {
        id: 1,
        name_en: 'Dumbbells',
        name_tr: 'Dumbbells',
        img: images.equipments.dumbbell,

    },{
        id: 2,
        name_en: 'Barbell',
        name_tr: 'Barbell',
        img: images.equipments.barbell,

    },{
        id: 3,
        name_en: 'Kettlebells',
        name_tr: 'Kettlebells',
        img: images.equipments.kettlebells,

    },{
        id: 4,
        name_en: 'No Equipment',
        name_tr: 'No Equipment',
        img: images.equipments.no_equipment,

    },{
        id: 5,
        name_en: 'Pull-up Bar',
        name_tr: 'Pull-up Bar',
        img: images.equipments.pullup,

    },{
        id: 6,
        name_en: 'Medicine Ball',
        name_tr: 'Medicine Ball',
        img: images.equipments.medicineball,

    },
]

const exercises = [
    {
        targetMuscles: [ 9,8,5,4],
        id          : 1,
        title       : 'Renegade Row',
        reps        : 12,
        sets        : '4',
        rest        : '30 Sec',
        equipment   : 1,
        level       : 2,
        image       : 'exercise_1519883084.jpg',
        video       : 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',
        tips        : '<ul><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ul>',
        instructions: '<ol><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ol>'
    }, {
        targetMuscles: [ 8,4],
        id          : 2,
        title       : 'Elbow Plank Pike Jacks',
        reps        : 12,
        sets        : '3',
        rest        : '1 Min',
        equipment   : 4,
        level       : 2,
        image       : 'exercise_1519883483.jpg',
        video       : 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',
        tips        : '<ul><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ul>',
        instructions: '<ol><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ol>'
    }, {
        targetMuscles: [ 8, 9],
        id          : 3,
        title       : 'Full Plank Passe Twist',
        reps        : 12,
        sets        : '4',
        rest        : '30 Sec',
        equipment   : 4,
        level       : 1,
        image       : 'exercise_1519883704.jpg',
        video       : 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',
        tips        : '<ul><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ul>',
        instructions: '<ol><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ol>'
    }, {
        targetMuscles: [ 2],
        id          :4,
        title       :'Dumbbell Side Lunge',
        reps        : 10,
        sets        :'3',
        rest        :'1 Min',
        equipment   :1,
        level       :1,
        image       :'exercise_1519883956.jpg',
        video       : 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',
        tips        :'<ul><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ul>',
        instructions:'<ol><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ol>'
    }, {
        targetMuscles: [ 2 ],
        id          : 5,
        title       : 'Sumo Squat Swing',
        reps        : 10,
        sets        : '3',
        rest        : '30 Sec',
        equipment   : 3,
        level       : 1,
        image       : 'exercise_1519884158.jpg',
        video       : 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',
        tips        : '<ul><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ul>',
        instructions: '<ol><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ol>'
    }, {
        targetMuscles: [ 1,2,8],
        id          : 6,
        title       : 'Barbell Hang Pull',
        reps        : 10,
        sets        : '4',
        rest        : '1 Min',
        equipment   : 5,
        level       : 3,
        image       : 'exercise_1519884329.jpg',
        video       : 'https://firebasestorage.googleapis.com/v0/b/tienda-reada-fitness.appspot.com/o/videos%2F6.mp4?alt=media&token=3e7494e2-f94c-45db-8b06-2977516cd93f',
        tips        : '<ul><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ul>',
        instructions: '<ol><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ol>'
    }, {
        targetMuscles: [ 1],
        id          : 7,
        title       : 'Single-arm Neutral-grip Dumbbell Row',
        reps        : 10,
        sets        : '3',
        rest        : '30 Sec',
        equipment   : 1,
        level       : 1,
        image       : 'exercise_1519884492.jpg',
        video       : 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',
        tips        : '<ul><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ul>',
        instructions: '<ol><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ol>'
    }, {
        targetMuscles: [ 3,5,8,9,4],
        id          : 9,
        title       : 'Feet-elevated Side-to-Side Pushup',
        reps        : 10,
        sets        : '3',
        rest        : '1 Min',
        equipment   :  4,
        level       : 2,
        image       : 'exercise_1519884889.jpg',
        video       : 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',
        tips        : '<ul><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ul>',
        instructions: '<ol><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ol>'
    },{
        targetMuscles: [ 3,5,8,9,4],
        id          : 8,
        title       : 'Feet-declined Side-to-Side Pushup',
        reps        : 10,
        sets        : '3',
        rest        : '1 Min',
        equipment   :  4,
        level       : 2,
        image       : 'exercise_1519884889.jpg',
        video       : 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',
        tips        : '<ul><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ul>',
        instructions: '<ol><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ol>'
    }, {
        targetMuscles: [ 8,9, 10 ],
        id          : 10,
        title       : 'Plank with Arm Raise',
        reps        : 12,
        sets        : '3',
        rest        : '30 Sec',
        equipment   :4,
        level       : 2,
        image       : 'exercise_1519938568.jpg',
        video       : 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',
        tips        : '<ul><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ul>',
        instructions: '<ol><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ol>'
    }, {
        targetMuscles: [ 2 ],
        id          : 11,
        title       : 'Partial Single-leg Squat',
        reps        : 15,
        sets        : '3',
        rest        : '45 Sec',
        equipment   : 4,
        level       : 2,
        image       : 'exercise_1519938967.jpg',
        video       : 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',
        tips        : '<ul><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ul>',
        instructions:'<ol><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ol>'
    }, {
        targetMuscles: [ 9, 5, 3],
        id          : 12,
        title       : 'EZ-Bar Pullover',
        reps        : 10,
        sets        : '4',
        rest        :'45 Sec',
        equipment   : 2,
        level       : 1,
        image       : 'exercise_1519939226.jpg',
        video       : 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',
        tips        : '<ul><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ul>',
        instructions: '<ol><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ol>'
    }, {
        targetMuscles: [10,9,5,3 ],
        id          : 13,
        title       : 'EZ-Bar Overhead Triceps',
        reps        :  12,
        sets        :  '3',
        rest        :  '1 Min',
        equipment   :  2,
        level       :  1,
        image       :  'exercise_1519939489.jpg',
        video       :  'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',
        tips        :  '<ul><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ul>',
        instructions:'<ol><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ol>'
    }, {
        targetMuscles: [8,9,10 ],
        id          :  14,
        title       :  'Cross Punch Roll-up',
        reps        :  10,
        sets        :  '4',
        rest        :  '1 Min',
        equipment   :  4,
        level       : 1,
        image       : 'exercise_1519940111.jpg',
        video       : 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',
        tips        : '<ul><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ul>',
        instructions:'<ol><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ol>'
    }, {
        targetMuscles: [ 9,5,1],
        id          :15,
        title       : 'Single-arm Medicine Ball Pushup',
        reps        : 8,
        sets        : '3',
        rest        :  '45 Sec',
        equipment   :  6,
        level       :  4,
        image       : 'exercise_1519940316.jpg',
        video       : 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',
        tips        : '<ul><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ul>',
        instructions: '<ol><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ol>'
    }, {
        targetMuscles: [8,2 ],
        id          : 16,
        title       :  'Glutes Stretch',
        reps        :  12,
        sets        :  '3',
        rest        :  '1 Min',
        equipment   :  4,
        level       :  1,
        image       :  'exercise_1519940754.jpg',
        video       :  'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',
        tips        : '<ul><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ul>',
        instructions:  '<ol><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ol>'
    }, {
        targetMuscles: [ 8],
        id          : 17,
        title       : '90-degree Static Hold',
        reps        : 12,
        sets        : '3',
        rest        : '45 Sec',
        equipment   : 4,
        level       : 1,
        image       : 'exercise_1519940878.jpg',
        video       :'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',
        tips        :'<ul><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ul>',
        instructions:'<ol><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ol>'
    }, {
        targetMuscles: [ 1,2,8,9,10],
        id          :18,
        title       :'Barbell High Pull',
        reps        : 10,
        sets        :'4',
        rest        :'1 Min',
        equipment   : 2,
        level       :3,
        image       :'exercise_1519941525.jpg',
        video       :'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',
        tips        :'<ul><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ul>',
        instructions:'<ol><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ol>'
    }, {
        targetMuscles: [ 5,8,9],
        id          :19,
        title       :'Reclining Triceps Press',
        reps        : 15,
        sets        :'4',
        rest        :'45 Sec',
        equipment   : 4,
        level       :1,
        image       :'exercise_1519941887.jpg',
        video       :'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',
        tips        :'<ul><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ul>',
        instructions:'<ol><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ol>'
    }, {
        targetMuscles: [ 9,2],
        id          : 20,
        title       : 'Overhead Lunge with Medicine Ball',
        reps        : 12,
        sets        : '4',
        rest        : '1 Min',
        equipment   : 6,
        level       : 2,
        image       : 'exercise_1519942162.jpg',
        video       : 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',
        tips        : '<ul><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ul>',
        instructions: '<ol><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ol>'
    },{
        targetMuscles: [ 12 ],
        id          : 21,
        title       : 'Plank',
        reps        : '30 Sec',
        sets        : '4',
        rest        : '30 Sec',
        equipment   : 1,
        level       : 2,
        image       : 'exercise_1519883084.jpg',
        video       : 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',
        tips        : '<ul><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ul>',
        instructions: '<ol><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ol>'
    },{
        targetMuscles: [ 11 ],
        id          : 22,
        title       : 'Skip Rope',
        reps        : '5 Mins',
        sets        : '3',
        rest        : '30 Sec',
        equipment   : 1,
        level       : 2,
        image       : 'exercise_1519883084.jpg',
        video       : 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',
        tips        : '<ul><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ul>',
        instructions: '<ol><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ol>'
    },{
        targetMuscles: [ 11 ],
        id          : 23,
        title       : 'Jumping Jacks',
        reps        : '60 Sec',
        sets        : '5',
        rest        : '30 Sec',
        equipment   : 1,
        level       : 2,
        image       : 'exercise_1519883084.jpg',
        video       : 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',
        tips        : '<ul><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ul>',
        instructions: '<ol><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ol>'
    },{
        targetMuscles: [ 11 ],
        id          : 24,
        title       : 'Treadmill Sprints',
        reps        : '60 Sec',
        sets        : '5',
        rest        : '30 Sec',
        equipment   : 1,
        level       : 2,
        image       : 'exercise_1519883084.jpg',
        video       : 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',
        tips        : '<ul><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ul>',
        instructions: '<ol><li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li><li>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</li><li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li></ol>'
    },

];

const programs = [
    {
        id              : 1,
        title           : 'The Ultimate Dumbbell-only Biceps Workout',
        description     : '<p>When someone asks you to make a muscle, chances are you don&rsquo;t flex your traps or rise onto your toes to show off your calves. You&#39;re going to roll up your sleeves and flex your biceps, inviting onlookers to your own personal &ldquo;gun show.&rdquo;</p><p>And while those arm-focused articles can prove helpful, many seem to present the same basic information, which can only take your gains so far. In an effort to help you bust through your biceps-building plateaus, we&#39;ve got a unique approach to promote new growth for those all-important show muscles.&nbsp;</p>',
        goal            : 4,
        level           : 2,
        duration        : '3',
        featured        : '1',
        img             : 'workout_1519944619.jpg'
    }, {
        id: 2,
        title           : 'The 6-week Triphasic Plan for Power',
        description     : '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p><p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p><p>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>',
        goal            : 4,
        level           : 2,
        duration        : '4',
        featured        :  '1',
        img             :  'workout_1519945666.jpg'
    }, {
        id: 3,
        title           : 'Hardcore CrossFit Workouts',
        description     : '<p>When someone asks you to make a muscle, chances are you don&rsquo;t flex your traps or rise onto your toes to show off your calves. You&#39;re going to roll up your sleeves and flex your biceps, inviting onlookers to your own personal &ldquo;gun show.&rdquo;</p><p>And while those arm-focused articles can prove helpful, many seem to present the same basic information, which can only take your gains so far. In an effort to help you bust through your biceps-building plateaus, we&#39;ve got a unique approach to promote new growth for those all-important show muscles.&nbsp;</p>',
        goal            : 3,
        level           : 2,
        duration        : '4',
        featured        : '1',
        img             : 'workout_1519945022.jpg'
    }, {
        id: 4,
        title           : 'The Sling Shot Bench Press Plan',
        description     : '<p>When someone asks you to make a muscle, chances are you don&rsquo;t flex your traps or rise onto your toes to show off your calves. You&#39;re going to roll up your sleeves and flex your biceps, inviting onlookers to your own personal &ldquo;gun show.&rdquo;</p><p>And while those arm-focused articles can prove helpful, many seem to present the same basic information, which can only take your gains so far. In an effort to help you bust through your biceps-building plateaus, we&#39;ve got a unique approach to promote new growth for those all-important show muscles.&nbsp;</p>',
        goal            :  3,
        level           : 3,
        duration        : '3',
        featured        : '1',
        img             : 'workout_1519945561.jpg'
    }, {
        id: 5,
        title           : 'The Ultimate Bruce Lee Training Program',
        description     : '<p>When someone asks you to make a muscle, chances are you don&rsquo;t flex your traps or rise onto your toes to show off your calves. You&#39;re going to roll up your sleeves and flex your biceps, inviting onlookers to your own personal &ldquo;gun show.&rdquo;</p><p>And while those arm-focused articles can prove helpful, many seem to present the same basic information, which can only take your gains so far. In an effort to help you bust through your biceps-building plateaus, we&#39;ve got a unique approach to promote new growth for those all-important show muscles.&nbsp;</p>',
        goal            :  4,
        level           : 3,
        duration        : '4',
        featured        : '1',
        img             : 'workout_1519945908.jpg'
    }, {
        id: 6,
        title           : 'The 100,000-Pound Challenge',
        description     : '<p>When someone asks you to make a muscle, chances are you don&rsquo;t flex your traps or rise onto your toes to show off your calves. You&#39;re going to roll up your sleeves and flex your biceps, inviting onlookers to your own personal &ldquo;gun show.&rdquo;</p><p>And while those arm-focused articles can prove helpful, many seem to present the same basic information, which can only take your gains so far. In an effort to help you bust through your biceps-building plateaus, we&#39;ve got a unique approach to promote new growth for those all-important show muscles.&nbsp;</p>',
        goal            : 2,
        level           : 3,
        duration        : '5',
        featured        : '1',
        img             : 'workout_1519946537.jpg'
    }, {
        id: 7,
        title           : 'Army Ranger Workout',
        description     : '<p>When someone asks you to make a muscle, chances are you don&rsquo;t flex your traps or rise onto your toes to show off your calves. You&#39;re going to roll up your sleeves and flex your biceps, inviting onlookers to your own personal &ldquo;gun show.&rdquo;</p><p>And while those arm-focused articles can prove helpful, many seem to present the same basic information, which can only take your gains so far. In an effort to help you bust through your biceps-building plateaus, we&#39;ve got a unique approach to promote new growth for those all-important show muscles.&nbsp;</p>',
        goal            : 4,
        level           : 4,
        duration        : '4',
        featured        : '1',
        img             : 'workout_1519947040.jpg'
    }, {
        id: 8,
        title           : 'The Ultimate Conditioning Workout',
        description     : '<p>When someone asks you to make a muscle, chances are you don&rsquo;t flex your traps or rise onto your toes to show off your calves. You&#39;re going to roll up your sleeves and flex your biceps, inviting onlookers to your own personal &ldquo;gun show.&rdquo;</p><p>And while those arm-focused articles can prove helpful, many seem to present the same basic information, which can only take your gains so far. In an effort to help you bust through your biceps-building plateaus, we&#39;ve got a unique approach to promote new growth for those all-important show muscles.&nbsp;</p>',
        goal            :  3,
        level           : 3,
        duration        : '3',
        featured        : '1',
        img             : 'workout_1519947305.jpg'
    }, {
        id: 9,
        title           : 'The 6-Week Fat Blast',
        description     : '<p>When someone asks you to make a muscle, chances are you don&rsquo;t flex your traps or rise onto your toes to show off your calves. You&#39;re going to roll up your sleeves and flex your biceps, inviting onlookers to your own personal &ldquo;gun show.&rdquo;</p><p>And while those arm-focused articles can prove helpful, many seem to present the same basic information, which can only take your gains so far. In an effort to help you bust through your biceps-building plateaus, we&#39;ve got a unique approach to promote new growth for those all-important show muscles.&nbsp;</p>',
        goal            : 1,
        level           :  2,
        duration        : '3',
        featured        : '1',
        img             : 'workout_1519947591.jpg'
    }, {
        id: 10,
        title           : 'The 500-rep Challenge Routine',
        description     : '<p>When someone asks you to make a muscle, chances are you don&rsquo;t flex your traps or rise onto your toes to show off your calves. You&#39;re going to roll up your sleeves and flex your biceps, inviting onlookers to your own personal &ldquo;gun show.&rdquo;</p><p>And while those arm-focused articles can prove helpful, many seem to present the same basic information, which can only take your gains so far. In an effort to help you bust through your biceps-building plateaus, we&#39;ve got a unique approach to promote new growth for those all-important show muscles.&nbsp;</p>',
        goal            : 4,
        level           : 4,
        duration        :  '4',
        featured        :  '1',
        img             :  'workout_1519948742.jpg'
    }, {
        id: 11,
        title           : 'Ageless Muscle: Total-Body Workout',
        description     : '<p>When someone asks you to make a muscle, chances are you don&rsquo;t flex your traps or rise onto your toes to show off your calves. You&#39;re going to roll up your sleeves and flex your biceps, inviting onlookers to your own personal &ldquo;gun show.&rdquo;</p><p>And while those arm-focused articles can prove helpful, many seem to present the same basic information, which can only take your gains so far. In an effort to help you bust through your biceps-building plateaus, we&#39;ve got a unique approach to promote new growth for those all-important show muscles.&nbsp;</p>',
        goal            : 2,
        level           :  4,
        duration        :  '4',
        featured        :  '1',
        img             :  'workout_1519948891.jpg'
    }, {
        id: 12,
        title           : 'Level Up Your Triceps Routine',
        description     : '<p>When someone asks you to make a muscle, chances are you don&rsquo;t flex your traps or rise onto your toes to show off your calves. You&#39;re going to roll up your sleeves and flex your biceps, inviting onlookers to your own personal &ldquo;gun show.&rdquo;</p><p>And while those arm-focused articles can prove helpful, many seem to present the same basic information, which can only take your gains so far. In an effort to help you bust through your biceps-building plateaus, we&#39;ve got a unique approach to promote new growth for those all-important show muscles.&nbsp;</p>',
        goal            : 2,
        level           : 4,
        duration        :  '5',
        featured        :  '1',
        img             :  'workout_1519949368.jpg'
    }, {
        id: 13,
        title           : 'Top It Off With This Full-Body Finisher',
        description     : '<p>When someone asks you to make a muscle, chances are you don&rsquo;t flex your traps or rise onto your toes to show off your calves. You&#39;re going to roll up your sleeves and flex your biceps, inviting onlookers to your own personal &ldquo;gun show.&rdquo;</p><p>And while those arm-focused articles can prove helpful, many seem to present the same basic information, which can only take your gains so far. In an effort to help you bust through your biceps-building plateaus, we&#39;ve got a unique approach to promote new growth for those all-important show muscles.&nbsp;</p>',
        goal            :  1,
        level           :  1,
        duration        :  '3',
        featured        :  '1',
        img             :  'workout_1519949759.jpg'
    }, {
        id: 14,
        title           : 'Real Man\'s Cardio Workout',
        description     : '<p>When someone asks you to make a muscle, chances are you don&rsquo;t flex your traps or rise onto your toes to show off your calves. You&#39;re going to roll up your sleeves and flex your biceps, inviting onlookers to your own personal &ldquo;gun show.&rdquo;</p><p>And while those arm-focused articles can prove helpful, many seem to present the same basic information, which can only take your gains so far. In an effort to help you bust through your biceps-building plateaus, we&#39;ve got a unique approach to promote new growth for those all-important show muscles.&nbsp;</p>',
        goal            : 1,
        level           :  1,
        duration        :  '4',
        featured        :  '1',
        img             :  'workout_1519949966.jpg'
    }, {
        id: 15,
        title           : 'The Ultimate Full-Body Landmine Workout',
        description     : '<p>When someone asks you to make a muscle, chances are you don&rsquo;t flex your traps or rise onto your toes to show off your calves. You&#39;re going to roll up your sleeves and flex your biceps, inviting onlookers to your own personal &ldquo;gun show.&rdquo;</p><p>And while those arm-focused articles can prove helpful, many seem to present the same basic information, which can only take your gains so far. In an effort to help you bust through your biceps-building plateaus, we&#39;ve got a unique approach to promote new growth for those all-important show muscles.&nbsp;</p>',
        goal            :  1,
        level           :  1,
        duration        :  '3',
        featured        :   '1',
        img             :  'workout_1519950276.jpg'
    }, {
        id: 16,
        title           :  'At-Home Cardio for Fat Loss',
        description     :  '<p>When someone asks you to make a muscle, chances are you don&rsquo;t flex your traps or rise onto your toes to show off your calves. You&#39;re going to roll up your sleeves and flex your biceps, inviting onlookers to your own personal &ldquo;gun show.&rdquo;</p><p>And while those arm-focused articles can prove helpful, many seem to present the same basic information, which can only take your gains so far. In an effort to help you bust through your biceps-building plateaus, we&#39;ve got a unique approach to promote new growth for those all-important show muscles.&nbsp;</p>',
        goal            :  3,
        level           :  1,
        duration        :  '4',
        featured        :  '1',
        img             :  'workout_1519950433.jpg'
    }
]

const levels = [
    {id: 1, title: 'Beginner', img: 'level_1516827206.jpg'},
    {id: 2, title: 'Intermediate',  img: 'level_1516827220.jpg'},
    {id: 3, title: 'Advanced',  img: 'level_1516827226.jpg'},
    {id: 4, title: 'Elite',  img: 'level_1517336508.jpg'}
]

const goals = [
    {id: 1, title: 'Fat Loss', img: 'goal_1516827131.jpg'},
    {id: 2, title: 'Build Muscle', img: 'goal_1516827137.jpg'},
    {id: 3, title: 'Transform', img: 'goal_1516827143.jpg'},
    {id: 4, title: 'Strength', img: 'goal_1517335949.jpg'}
]

const programPlans = [
    {
    program_id : 1,
    plan: [
        { day: 1, exercises: [1,2,3] },
        { day: 2, exercises: [1,2,3] },
        { day: 3, exercises: [1,2,3] },
        { day: 4, exercises: [3,6,9] },
        { day: 5, exercises: [3,6,9] },
        { day: 6, exercises: [5,8,1,9]},
        { day: 7, exercises: [5,8,1,9]},
    ]
}, {
    program_id : 2,
    plan: [
        { day: 1, exercises: [1,5,12,20] },
        { day: 2, exercises: [21,22,3,8] },
        { day: 3, exercises: [21,22,3,8] },
        { day: 4, exercises: [4,1,6,17] },
        { day: 5, exercises: [4,1,6,17] },
        { day: 6, exercises: [4,1,6,17] },
        { day: 7, exercises: [4,1,2] },
    ]
}, {
    program_id : 3,
    plan: [
        { day: 1, exercises: [2,6,12,16] },
        { day: 2, exercises: [2,6,12,16] },
        { day: 3, exercises: [3,7,9,11] },
        { day: 4, exercises: [4,7,1,5] },
        { day: 5, exercises: [4,7,1,5] },
        { day: 6, exercises: [13,17,18] },
        { day: 7, exercises: [13,17,18] },
    ]
}, {
    program_id : 4,
    plan: [
        { day: 1, exercises: [7,13,6,4] },
        { day: 2, exercises: [6,4,12,13] },
        { day: 3, exercises: [12,16,5] },
        { day: 4, exercises: [11,14,8] },
        { day: 5, exercises: [11,14,8] },
        { day: 6, exercises: [11,14,8] },
        { day: 7, exercises: [11,14,8] },
    ]
}, {
    program_id : 5,
    plan: [
        { day: 1, exercises: [1,3,5,6] },
        { day: 2, exercises: [6,11,17,19] },
        { day: 3, exercises: [6,11,17,19] },
        { day: 4, exercises: [6,11,17,19] },
        { day: 5, exercises: [17, 4, 3] },
        { day: 6, exercises: [11,13,11] },
        { day: 7, exercises: [11,13,11] },
    ]
}, {
    program_id : 6,
    plan: [
        { day: 1, exercises: [4,3,5,9] },
        { day: 2, exercises: [4,3,5,9] },
        { day: 3, exercises: [9,5,11,14] },
        { day: 4, exercises: [9,5,11,14] },
        { day: 5, exercises: [3,16] },
        { day: 6, exercises: [3,16] },
        { day: 7, exercises: [3,16] },
    ]
}, {
    program_id : 7,
    plan: [
        { day: 1, exercises: [2,3,5,11] },
        { day: 2, exercises: [5,7,9] },
        { day: 3, exercises: [5,7,9] },
        { day: 4, exercises: [8,1,5] },
        { day: 5, exercises: [8,1,5] },
        { day: 6, exercises: [8,1,5] },
        { day: 7, exercises: [3,4,7,8] },
    ]
}, {
    program_id : 8,
    plan: [
        { day: 1, exercises: [4,6,17,2] },
        { day: 2, exercises: [17,2] },
        { day: 3, exercises: [17,2] },
        { day: 4, exercises: [17,2] },
        { day: 5, exercises: [11,16,1] },
        { day: 6, exercises: [18,21,6] },
        { day: 7, exercises: [18,21,6] },
    ]
}, {
    program_id : 9,
    plan: [
        { day: 1, exercises: [2,4,6,7] },
        { day: 2, exercises: [2,4,6,7] },
        { day: 3, exercises: [6,7] },
        { day: 4, exercises: [2,1,4] },
        { day: 5, exercises: [2,1,4] },
        { day: 6, exercises: [2,1,4] },
        { day: 7, exercises: [11,7] },
    ]
}, {
    program_id : 10,
    plan: [
        { day: 1, exercises: [3,6,14,16] },
        { day: 2, exercises: [6,14,16] },
        { day: 3, exercises: [6,14,16] },
        { day: 4, exercises: [6,14,16] },
        { day: 5, exercises: [14,16] },
        { day: 6, exercises: [3,14] },
        { day: 7, exercises: [3,14] },
    ]
}, {
    program_id : 11,
    plan: [
        { day: 1, exercises: [9,4,3] },
        { day: 2, exercises: [9,4,8] },
        { day: 3, exercises: [9,4,8] },
        { day: 4, exercises: [1,6] },
        { day: 5, exercises: [6,8] },
        { day: 6, exercises: [6,8] },
        { day: 7, exercises: [21,22,3] },
    ]
}, {
    program_id : 12,
    plan: [
        { day: 1, exercises: [11,6,2,5,17] },
        { day: 2, exercises: [1,6,8,5,17] },
        { day: 3, exercises: [1,6,8,5,17] },
        { day: 4, exercises: [11,17] },
        { day: 5, exercises: [11,17] },
        { day: 6, exercises: [6,5,17] },
        { day: 7, exercises: [2,6,5] },
    ]
}, {
    program_id : 13,
    plan: [
        { day: 1, exercises: [1,6,4,5] },
        { day: 2, exercises: [1,3,4,9] },
        { day: 3, exercises: [1,4] },
        { day: 4, exercises: [1,6] },
        { day: 5, exercises: [6,5] },
        { day: 5, exercises: [6,7,9] },
        { day: 7, exercises: [1,6,4,5] },
    ]
}, {
    program_id : 14,
    plan: [
        { day: 1, exercises: [1,3,9,5] },
        { day: 2, exercises: [1,3,7,5] },
        { day: 3, exercises: [5,3,2] },
        { day: 5, exercises: [9,5,2] },
        { day: 6, exercises: [3,1,5] },
        { day: 7, exercises: [9,2] },
    ]
}, {
    program_id : 15,
    plan: [
        { day: 1, exercises: [5,15,16,17] },
        { day: 2, exercises: [3,4,7,10] },
        { day: 3, exercises: [3,4,8,10] },
        { day: 4, exercises: [7,15,17] },
        { day: 5, exercises: [7,14,17] },
        { day: 6, exercises: [15,3,4] },
        { day: 6, exercises: [1,3,4] },
    ]
}, {
    program_id : 16,
    plan: [
        { day: 1, exercises: [2,3,5] },
        { day: 2, exercises: [2,3,1] },
        { day: 3, exercises: [16,10,3] },
        { day: 4, exercises: [3,2,5] },
        { day: 5, exercises: [16,3] },
        { day: 6, exercises: [2,3,5] },
        { day: 7, exercises: [1,3,5] },
    ]
}]

export {equipments, exercises, muscles, programs, levels, goals, programPlans}
