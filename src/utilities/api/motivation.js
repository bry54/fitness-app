import * as moment from "moment";

const quotes =[
    {
        id: 1,
        quote_en:  'Pain is weakness leaving the body',
        owner: 'Some owner name'
    }, {
        id: 2,
        quote_en: 'Being defeated is often a temporary condition. Giving up is what makes it permanent',
        owner: 'Some owner name'
    }, {
        id: 3,
        quote_en:  'Failure is only a temporary change in direction to set you straight for your next success',
        owner: 'Some owner name'
    }, {
        id: 4,
        quote_en:  'The worst thing you can be is average',
        owner: 'Some owner name'
    }, {
        id: 5,
        quote_en:  'When it starts to hurt, thats when the set starts',
        owner: 'Some owner name'
    }, {
        id: 6,
        quote_en:  'To achieve something you’ve never had before, you must do something you’ve never done before',
        owner: 'Some owner name'
    }, {
        id: 7,
        quote_en:  'You don’t demand respect, you earn it',
        owner: 'Some owner name'
    }, {
        id: 8,
        quote_en:  'Expecting the world to treat you fairly because you’re an honest person is like expecting the bull not to charge you because you’re a vegetarian',
        owner: 'Some owner name'
    }, {
        id: 9,
        quote_en:  'Be proud, but never satisfied',
        owner: 'Some owner name'
    }, {
        id: 10,
        quote_en:  'Obsession is what lazy people call dedication',
        owner: 'Some owner name'
    }, {
        id: 11,
        quote_en:  'The best way to predict your future is to create it',
        owner: 'Some owner name'
    }
];

const stories = [
    {
        id: 1,
        author: 'Brian Sithole',
        date: '2019-06-01',
        title: 'Lost 10kg in 100 days',
        story: 'Hercle, cacula albus!.Fidess sunt parss de dexter extum.Assimilatios sunt clabulares de grandis sensorem.Danista, cacula, et imber.Favere inciviliter ducunt ad albus genetrix.Gabalium potuss, tanquam neuter acipenser.Cur zeta accelerare?',
        language: 'en',
        img: ''
    }, {
        id: 2,
        author: 'Marshal Mangondoza',
        date: '2020-03-30',
        title: 'Enjoying my best physical form',
        story: 'Hercle, cacula albus!.Fidess sunt parss de dexter extum.Assimilatios sunt clabulares de grandis sensorem.Danista, cacula, et imber.Favere inciviliter ducunt ad albus genetrix.Gabalium potuss, tanquam neuter acipenser.Cur zeta accelerare?',
        language: 'en',
        img: ''
    }, {
        id: 3,
        author: 'Farai Kaila',
        date: '2020-04-30',
        title: 'My journey to healthy weight gains. UNDEFEATED',
        story: 'Hercle, cacula albus!.Fidess sunt parss de dexter extum.Assimilatios sunt clabulares de grandis sensorem.Danista, cacula, et imber.Favere inciviliter ducunt ad albus genetrix.Gabalium potuss, tanquam neuter acipenser.Cur zeta accelerare?',
        language: 'en',
        img: ''
    }
]

export {quotes, stories}
