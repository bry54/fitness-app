import thunk from 'redux-thunk';
import {AsyncStorage} from "react-native";
import {createMigrate, persistReducer, persistStore} from 'redux-persist'
import {applyMiddleware, combineReducers, compose, createStore} from "redux";

import {initialState as initialAuthReducer, reducer as authReducer} from '../../store/reducers/auth'
import {initialState as initialGlobalReducer, reducer as globalReducer} from '../../store/reducers/global'

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const migrations = {
    // Migration Version in production: 0, increase by 1 when publishing a modified store!!!
    2: (state) => {
        return {
            // reinitialize modified reducers.
            ...state,
            authReducer: initialAuthReducer,
            globalReducer: initialGlobalReducer,
        }
    },
};

const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
    timeout: null,
    version: 2,
    debug: false,
    migrate: createMigrate(migrations, { debug: false }),
};

const rootReducer = combineReducers({
    globalReducer: globalReducer,
    authReducer: authReducer,
});

const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = createStore(persistedReducer, composeEnhancers(applyMiddleware(thunk)));
const  persistor = persistStore(store);

export { store, persistor };
