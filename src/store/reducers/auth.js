import {AUTH_ATTEMPT_SIGN_UP, AUTH_NULLIFY_LOGGED_IN_USER, AUTH_SET_LOGGED_IN_USER,} from "../utilities/actionTypes";

const initialState = {
    loggedInUser: null,
};

const reducer = (state = initialState, action) => {
    switch (action.type) {

        case AUTH_SET_LOGGED_IN_USER: {
            const user = action.payload;

            return {
                ...state,
                loggedInUser: user
            };
        }

        case AUTH_NULLIFY_LOGGED_IN_USER: {
            return {
                ...state,
                loggedInUser: null
            };
        }

        default:
            return state;
    }
};

export {initialState, reducer};
