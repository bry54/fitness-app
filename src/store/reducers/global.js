import {UPDATE_GLOBAL_CONTROLS} from "../utilities/actionTypes";
import i18n from "i18n-js";
import moment from "moment";

const initialState = {
    language: 'en',
    currency: 'TRY',
    defaultProgram: null,
    viewedOnBoardingScreen: false,
    resourcesCached: false,
    useAsGuest: false,
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_GLOBAL_CONTROLS:{
            const controlObject = action.payload;
            const key = controlObject.key;
            const value = controlObject.value;

            if (key === 'language') {
                i18n.locale = value;
                moment.locale(value);
            }

            return {
                ...state,
                [key] : value
            };
        }

        default:
            return state;
    }
};

export {initialState, reducer};
