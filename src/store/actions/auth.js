import {AUTH_ATTEMPT_SIGN_UP, AUTH_NULLIFY_LOGGED_IN_USER, AUTH_SET_LOGGED_IN_USER} from '../utilities/actionTypes';

export const attemptSignIn = (credentials, DEMO=false, user=null) => async dispatch => {
    let defaultUser = {"id":1,"name":{"title":"Mr","first":"Samuel","middle":"Tadiwa","last":"Kuzakwawo"},"particulars":{"dob":"23.12.2001","gender":"M","picture":{"large":"https://robohash.org/blanditiistotamqui.jpg?size=50x50&set=set1","medium":"https://robohash.org/harumdolordolores.jpg?size=50x50&set=set1","thumbnail":"https://robohash.org/etmollitianesciunt.bmp?size=50x50&set=set1"}},"contacts":{"email":"samkuzah@muzukuru.com","phone":"(913) 8719202","cell":"+1 202 216 6439","location":{"street":"59 Norway Maple Pass","city":"Shawnee Mission","province_state":"Kansas","country":"United States","postcode":"66225"}},"login":{"uuid":"a901d8fa-5405-4c1d-b224-8aa21d6fd26e","membership_number":"SK-911702","password":"secret","md5":"5fccbfd762d2d4e5ce087063a8757873"},"nationality":"US","payment_methods":[{"type":"visa","card_number":"3553980455424950","expiry_date":"11.02.2020","holder_name":"SAMUEL TADIWA KUZAKWAWO"},{"type":"discover","card_number":"4936233207520170","expiry_date":"16.06.2019","holder_name":"SAMUEL TADIWA KUZAKWAWO"},{"type":"mastercard","card_number":"3583056800565385","expiry_date":"05.11.2020","holder_name":"SAMUEL TADIWA KUZAKWAWO"}]}

    let loggedInUser = null
    if (DEMO){
        loggedInUser = defaultUser
    } else if (user){
        loggedInUser = user
    } else {
        console.log('here...')
        if ((credentials.membershipNumber === defaultUser.login.membership_number.toLowerCase())  && (credentials.password === defaultUser.login.password)){
            loggedInUser = defaultUser
        }
    }

    dispatch({
        type: AUTH_SET_LOGGED_IN_USER,
        payload: loggedInUser
    });
};

export const attemptSignUp = (user) => async dispatch => {
    dispatch(attemptSignIn(null, false, user));
};

export const attemptSignOut = () => async dispatch => {
    dispatch({
        type: AUTH_NULLIFY_LOGGED_IN_USER,
        payload: null
    });
};
