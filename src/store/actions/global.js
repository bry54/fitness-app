import { UPDATE_GLOBAL_CONTROLS} from '../utilities/actionTypes';
import * as Font from 'expo-font';
import {Asset} from 'expo-asset';
import {Ionicons, MaterialCommunityIcons} from '@expo/vector-icons';
import {appImages} from "../../assets/images";

export const updateGlobalControls = (controlObject) => dispatch => {
    dispatch({
        type: UPDATE_GLOBAL_CONTROLS,
        payload: controlObject
    });
};

const cacheImages = (images) => {
    return images.map(image => {
        if (typeof image === 'string') {
            return Image.prefetch(image);
        } else {
            return Asset.fromModule(image).downloadAsync();
        }
    });
};

const cacheFonts = (fonts) => {
    return fonts.map(font => Font.loadAsync(font));
};

export const cacheResources = () => async dispatch => {
    let obj = {
        key: 'resourcesCached',
        value: false
    };
    const imageAssets = cacheImages(appImages);
    const fontAssets = cacheFonts([/*MaterialCommunityIcons.font, Ionicons.font*/]);
    const assetsCached = Promise.all([...imageAssets, ...fontAssets]);

    await assetsCached
        .then(res => { obj.value = true})
        .catch(error => { obj.value = false});

    dispatch({
        type: UPDATE_GLOBAL_CONTROLS,
        payload: obj
    });
};
