import {quotes} from "../../utilities/api/motivation";
import {ImageBackground, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import React from "react";
import ColorDefinitions from "../../constants/ColorDefinitions";
import images from "../../assets/images";

export default (props) => {
    const quote = props.quote
    return (
        <ImageBackground source={images.motivation_quotes.story} style={styles.quoteContainer}>
            <View>
                <Text style={styles.quoteText}>"{quote.quote_en}"</Text>
                <Text style={styles.sourceText}> - {quote.owner}</Text>
            </View>
            <TouchableOpacity style={styles.button} onPress={() => props.getNextQuote()}>
                <Text style={styles.buttonText}>Next</Text>
            </TouchableOpacity>
        </ImageBackground>
    )
}

const styles = StyleSheet.create({
    quoteContainer: {
        flex: 1,
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 20
    },
    quoteText: {
        fontFamily: 'alpha-regular',
        fontSize: 36,
        color: 'rgba(0, 0, 0, .8)',
        marginVertical: 30,
    },
    sourceText: {
        fontFamily: 'handwriting',
        fontSize: 25,
        color: '#F8F8F8',
        textAlign: 'right',
        marginVertical: 30
    },

    button: {
        borderWidth: 2,
        borderColor: ColorDefinitions.primaryText.shade0,
        width: 200,
        padding: 10,
        marginBottom: 20,
        bottom:10
    },
    buttonText: {
        textAlign: 'center',
        color: ColorDefinitions.primaryText.shade0,
        fontSize: 18,
    }
});
