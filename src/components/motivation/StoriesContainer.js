import {StyleSheet, Text, TouchableOpacity, View} from "react-native";
import React from "react";
import ColorDefinitions from "../../constants/ColorDefinitions";
import {ListItem} from "react-native-elements";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import moment from "moment";

export default (props) => {
    const stories = props.stories
    return (
        <View style={{marginVertical: 10}}>
            {stories.map((story, index) => (
                <ListItem
                    key={index}
                    onPress={() => props.loadStory(story)}
                    title={story.title}
                    containerStyle={{marginHorizontal: 0, marginVertical: 5, backgroundColor: ColorDefinitions.primaryBackground.shade0, borderRadius: 0, elevation: 5}}
                    titleStyle={{fontSize: 20,fontFamily: 'alpha-regular', color: ColorDefinitions.primaryText.shade0, marginBottom: 10}}
                    leftAvatar={{
                        size: 'medium',
                        title: story.author.match(/\b(\w)/g).join('.'),
                        titleStyle: {fontFamily: 'alpha-regular', color: ColorDefinitions.secondaryText.shade0},
                        rounded: false,
                        overlayContainerStyle: {backgroundColor: ColorDefinitions.secondaryBackground.shade0, borderRadius: 7}
                    }}
                    subtitle={'- ' + story.author + ' \n -'+moment(story.date).fromNow()}
                    subtitleStyle={{fontFamily: 'alpha-regular',color: ColorDefinitions.primaryText.shade0}}
                    chevron={<MaterialCommunityIcons name={'chevron-right'} color={ColorDefinitions.primaryText.shade0} size={16} />}/>
            ))}
        </View>
    )
}

const styles = StyleSheet.create({
    quoteContainer: {
        flex: 1,
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 20
    }
});
