import React, {PureComponent} from 'react';
import OptionsMenu from "react-native-options-menu";
import {MaterialCommunityIcons} from '@expo/vector-icons';
import {connect} from "react-redux";
import ColorDefinitions from "../../constants/ColorDefinitions";

class SettingsButton extends PureComponent{
    render() {
        const optionsIcon = (<MaterialCommunityIcons
            name='dots-vertical'
            color={ColorDefinitions.primaryText.shade0}
            size={30}
            style={{paddingHorizontal: 10}}/> );
        return (

            <OptionsMenu
                customButton={ optionsIcon }
                destructiveIndex={1}
                options={[
                    'Notifications',
                    'About'
                ]}
                actions={[
                    ()=>this._settingsActions( 'Notifications'),
                    ()=>this._settingsActions( 'AboutApp')]} />
        );
    }

    _settingsActions = (screen) => {
        //this.props.navigation.navigate(screen);
    };
}

const mapStateToProps = (state) => {
    return {
        //hasUnreadNotifications: state.firebaseReducer.notifications.receivedNotifications.find((notification) => notification.read === false)
    }
};

const matchDispatchToProps = dispatch => {
    return {

    }
};

export default connect(mapStateToProps, matchDispatchToProps)(SettingsButton);
