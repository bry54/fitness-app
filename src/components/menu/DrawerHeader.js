import {StyleSheet, Text, View} from "react-native";
import {Avatar, Divider} from "react-native-elements";
import React from "react";
import TypoDefinitions from "../../constants/TypoDefinitions";
import ColorDefinitions from "../../constants/ColorDefinitions";
import Ripple from "react-native-material-ripple";

export  default (props) => (
    <Ripple
        rippleOpacity={.8}
        style={{alignItems: 'center', justifyContent: 'center' }}
        onPress={()=>props.navHandler()}>

        <View style={styles.userInfoContainer}>
            <Avatar
                rounded
                title={props.initials}
                size='medium'
                containerStyle={{margin: 5, backgroundColor: 'transparent'}}
                overlayContainerStyle={{backgroundColor: ColorDefinitions.primaryText.shade0,}}
                titleStyle={{fontFamily: 'alpha-regular', color: ColorDefinitions.secondaryText.shade0}} />

            <View style={{paddingHorizontal:4, paddingVertical: 4}}>
                <Text style={styles.userName}>{props.fullName}</Text>
                {
                    props.userID ? (
                        <Text style={styles.userID}>{props.userID}</Text>
                    ) : (
                        <Text style={[styles.authButton]}>
                            SignIn or Create account
                        </Text>
                    )
                }
            </View>
        </View>
    </Ripple>
);

const styles = StyleSheet.create({
    userInfoContainer: {
        marginTop: -4,
        marginBottom: 0,
        padding: 20,
        width: '100%',
        flexDirection: 'row',
        backgroundColor: ColorDefinitions.primaryBackground.shade0
    },

    userName: {
        color: ColorDefinitions.primaryText.shade0,
        fontSize: TypoDefinitions.smallFont,
        fontWeight: TypoDefinitions.normalText,
        textTransform: 'capitalize',
        fontFamily: 'alpha-regular',
        paddingVertical: 5
    },

    userID: {
        color: ColorDefinitions.primaryText.shade0,
        fontFamily: 'numeric-regular',
        fontSize: TypoDefinitions.normalFont,
        paddingVertical: 0
    },

    authButton: {
        color: ColorDefinitions.primaryText.shade0,
        fontFamily: 'alpha-regular',
        paddingVertical: 0,
        fontSize: TypoDefinitions.xxSmallFont
    },
});
