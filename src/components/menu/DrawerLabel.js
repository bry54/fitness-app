import React, {PureComponent} from 'react';
import {StyleSheet, Text, View} from "react-native";
import {Badge, Divider, ListItem} from "react-native-elements";
import TypoDefinitions from "../../constants/TypoDefinitions";
import DrawerItem from "@react-navigation/drawer/src/views/DrawerItem";
import ColorDefinitions from "../../constants/ColorDefinitions";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import Ripple from "react-native-material-ripple";

export default class DrawerLabel extends PureComponent{
    render() {
        const item = this.props.item;
        return (
            <View>

                        <Ripple
                            key={item.route}
                            rippleOpacity={.8}
                            onPress={() => this.props.navHandler(item.route)}>
                        <ListItem
                            bottomDivider={false}
                            containerStyle={{backgroundColor: ColorDefinitions.colorPrimary, padding: 0}}
                            title={
                                <View>
                                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                                        <DrawerItem
                                            icon={() => <MaterialCommunityIcons name={item.icon} size={24}
                                                                                color={ColorDefinitions.primaryText.shade0}/>}
                                            style={{flex: 1}}
                                            label={() => (
                                                <Text style={styles.labelDefinition}>{item.label}</Text>
                                            )}
                                            activeBackgroundColor={ColorDefinitions.colorPrimaryDark}
                                            activeTintColor={ColorDefinitions.primaryText.shade0}
                                        />

                                        {item.count ? <Badge
                                            containerStyle={{margin: 10, marginTop: 18}}
                                            value={item.count}
                                            badgeStyle={{
                                                backgroundColor: ColorDefinitions.primaryBackground.shade0,
                                                borderColor: 'transparent',
                                                borderRadius: 3,
                                            }}/> : null}
                                    </View>
                                    <Divider style={{backgroundColor: ColorDefinitions.primaryText.shade0}}/>
                                </View>
                            }
                        />
                        </Ripple>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    labelDefinition: {
        color: ColorDefinitions.primaryText.shade0,
        fontSize: TypoDefinitions.normalFont,
        fontWeight: TypoDefinitions.normalText,
        textTransform: 'capitalize',
        fontFamily: 'alpha-regular'
    },
});
