import React, {PureComponent} from 'react';
import {Platform, View} from "react-native";
import {Ionicons} from '@expo/vector-icons';
import ColorDefinitions from "../../constants/ColorDefinitions";

export default class BackButton extends PureComponent{
    render() {
        return (
            <View style={{flex: 0, flexDirection: 'row'}}>
                <Ionicons
                    onPress={() => this._handleBackButton()}
                    name={Platform.OS === 'ios' ? 'ios-arrow-back' : 'md-arrow-back'}
                    color={ColorDefinitions.primaryText.shade0}
                    size={26}
                    style={{paddingLeft: 20}}/>
            </View>
        );
    }

    _handleBackButton = () => {
        this.props.navigation.goBack();
    }
}
