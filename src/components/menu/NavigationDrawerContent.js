import React, {PureComponent} from 'react';
import {SafeAreaView, Text, View} from "react-native";
import {DrawerContentScrollView} from '@react-navigation/drawer';
import DrawerHeader from "./DrawerHeader";
import DrawerLabel from "./DrawerLabel";
import {connect} from "react-redux";
import i18n from "../../localizations/i18n";
import ColorDefinitions from "../../constants/ColorDefinitions";
import SwitchSelector from "react-native-switch-selector";
import images from "../../assets/images";
import {Avatar} from "react-native-elements";
import {updateGlobalControls} from "../../store/actions/global";

class NavigationDrawerContent extends PureComponent{
    state ={
        language: 'en'
    }

    render () {
        const menuItems = [
            {icon: 'view-dashboard', label: i18n.t('menu_home'), route: 'Home', requiresLogin: false, count: 0},
            {icon: 'wunderlist', label: i18n.t('menu_workouts'), route: 'Workouts', requiresLogin: false, count: 0},
            {icon: 'dumbbell', label: i18n.t('menu_exercises'), route: 'Exercises', requiresLogin: false, count: 0},
            {icon: 'food-fork-drink', label: i18n.t('menu_diets'), route: 'Diets', requiresLogin: false, count: 0},
            {icon: 'format-quote-open', label: i18n.t('menu_motivation'), route: 'Motivation', requiresLogin: false, count: 0},
            {icon: 'account-badge', label: i18n.t('menu_profile'), route: 'MyProfile', requiresLogin: false, count: 0},
            {icon: 'shopping', label: i18n.t('menu_shop'), route: 'GymShop', requiresLogin: false, count: 0},
            //{icon: 'notebook', label: i18n.t('menu_logs'), route: 'Logs', requiresLogin: true, count: 0},
        ];

        const user = this.props.user;
        const guestMenu = menuItems.filter(menuItem=> !menuItem.requiresLogin);
        const shownMenu = user ? menuItems : guestMenu;

        return(
            <SafeAreaView style={{flex: 1}} forceInset={{ top: 'always', horizontal: 'never' }}>
                <DrawerContentScrollView style={{ }} {...this.props} showsVerticalScrollIndicator={false}>
                    <DrawerHeader
                        initials={user ? user.name.first.charAt(0)+user.name.last.charAt(0) : 'G'}
                        fullName={user ? user.name.first + ' ' +user.name.last : 'Guest User'}
                        userID={user ? user.login.membership_number : null}
                        navHandler={user ? this.navToMyProfile : this.navToAuthOptions}/>

                    {
                        shownMenu.map((_item) => (
                                <DrawerLabel
                                    user={user}
                                    key={JSON.stringify(_item)}
                                    item={_item}
                                    navHandler={this.navToDrawerScreen}/>
                            )
                        )
                    }

                    <View style={{marginVertical: 20}}>
                        <Avatar
                            containerStyle={{alignSelf: "center", marginTop: 10}}
                            rounded
                            size={'medium'}
                            source={images.logo}
                        />
                        <Text style={{fontFamily: 'numeric-regular', color: ColorDefinitions.primaryText.shade0, textAlign: 'center', fontSize: 12, paddingVertical: 10}}>{i18n.t('lbl_copyright')}</Text>
                    </View>
                </DrawerContentScrollView>
            </SafeAreaView>
        )
    }

    navToMyProfile = () =>{
        const {navigation} = this.props;
        navigation.navigate('MyProfile')
    };

    navToAuthOptions = () =>{
        const {navigation} = this.props;
        navigation.navigate('Auth')
    };

    navToDrawerScreen = async (routeName) =>{
        const {navigation} = this.props;
        navigation.navigate(routeName);
    };

}

const mapStateToProps = (state) => {
    return {
        user: state.authReducer.loggedInUser,
        language: state.globalReducer.language
    }
};

const matchDispatchToProps = dispatch => {
    return {
        updateGlobalControl: (controlObj) => dispatch(updateGlobalControls(controlObj))
    }
};

export default connect(mapStateToProps, matchDispatchToProps)(NavigationDrawerContent);
