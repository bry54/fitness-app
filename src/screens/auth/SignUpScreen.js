import React, {Component} from 'react';
import {ScrollView, StyleSheet, Switch, Text, TouchableOpacity, View} from 'react-native';
import {connect} from "react-redux";
import TypoDefinitions from "../../constants/TypoDefinitions";
import ColorDefinitions from "../../constants/ColorDefinitions";
import {Button, Input} from "react-native-elements";
import moment from "moment";
import {Appearance} from "react-native-appearance";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import i18n from "../../localizations/i18n";
import SwitchSelector from "react-native-switch-selector";
import {attemptSignUp} from "../../store/actions/auth";

class SignUpScreen extends Component {
    state={
        email: '',
        phone: '',
        password: '',
        passwordConfirm: '',
        givenNames: '',
        surname: '',
        membershipNumber: '',
        agreeTC: false,
        confirmCorrectness: false,
        dateOfBirth: new Date(),
        dobPickerVisible: false,
        gender: 'm'
    };

    render () {
        const colorScheme = Appearance.getColorScheme();
        return (
            <ScrollView
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                style={styles.container}>

                <DateTimePickerModal
                    isDarkModeEnabled={colorScheme === 'dark'}
                    isVisible={this.state.dobPickerVisible}
                    mode="date"
                    onConfirm={async (date) => {
                        await this._updateInputControl('dateOfBirth', date);
                        this._updateInputControl('dobPickerVisible', false)
                    }}
                    onCancel={() => this._updateInputControl('dobPickerVisible', false)}
                />

                <View style={styles.signUpSection}>
                    <Text style={styles.sectionHeading}>{i18n.t('lbl_personal_information')}</Text>
                    <Input
                        rightIcon={<MaterialCommunityIcons name={'rename-box'} size={20} color={ColorDefinitions.primaryText.shade0}/>}
                        value={this.state.givenNames}
                        containerStyle={{ paddingTop: 5}}
                        inputContainerStyle={{borderColor: ColorDefinitions.primaryText.shade0, borderBottomWidth:1, paddingTop: 0}}
                        inputStyle={{fontFamily: 'alpha-regular'}}
                        label={i18n.t('lbl_first_name')}
                        labelStyle={{fontFamily: 'alpha-regular', fontWeight: TypoDefinitions.primaryText, fontSize: TypoDefinitions.xSmallFont, color: ColorDefinitions.primaryText.shade0}}
                        placeholder='John'
                        onChangeText={val => this._updateInputControl('givenNames', val)}
                    />

                    <Input
                        rightIcon={<MaterialCommunityIcons name={'rename-box'} size={20} color={ColorDefinitions.primaryText.shade0}/>}
                        value={this.state.surname}
                        containerStyle={{ paddingTop: 5}}
                        inputContainerStyle={{borderColor: ColorDefinitions.primaryText.shade0, borderBottomWidth:1}}
                        inputStyle={{fontFamily: 'alpha-regular'}}
                        label={i18n.t('lbl_surname')}
                        labelStyle={{fontFamily: 'alpha-regular', fontWeight: TypoDefinitions.primaryText, fontSize: TypoDefinitions.xSmallFont, color: ColorDefinitions.colorAccent}}
                        placeholder='Doe'
                        onChangeText={val => this._updateInputControl('surname', val)}
                    />

                    <Input
                        rightIcon={<MaterialCommunityIcons name={'passport'} size={20} color={ColorDefinitions.primaryText.shade0}/>}
                        autoCapitalize='none'
                        value={this.state.membershipNumber}
                        containerStyle={{ paddingTop: 5}}
                        inputContainerStyle={{borderColor: ColorDefinitions.primaryText.shade0, borderBottomWidth:1}}
                        inputStyle={{fontFamily: 'alpha-regular'}}
                        label={i18n.t('lbl_membership_number')}
                        labelStyle={{fontFamily: 'alpha-regular', fontWeight: TypoDefinitions.primaryText, fontSize: TypoDefinitions.xSmallFont, color: ColorDefinitions.primaryText.shade0}}
                        placeholder='XX-######'
                        onChangeText={val => this._updateInputControl('membershipNumber', val)}
                    />

                    <TouchableOpacity
                        activeOpacity={.9}
                        onPress={() => {this._updateInputControl('dobPickerVisible', true)}}>
                        <Input
                            rightIcon={<MaterialCommunityIcons name={'calendar'} size={20} color={ColorDefinitions.primaryText.shade0}/>}
                            editable={false}
                            value={ moment(this.state.dateOfBirth).format('DD.MM.YYYY') }
                            containerStyle={{ paddingTop: 5}}
                            inputContainerStyle={{borderColor: ColorDefinitions.primaryText.shade0, borderBottomWidth:1}}
                            label={i18n.t('lbl_date_of_birth')}
                            labelStyle={{fontFamily: 'alpha-regular', fontWeight: TypoDefinitions.primaryText, fontSize: TypoDefinitions.xSmallFont, color: ColorDefinitions.primaryText.shade0}}
                            inputStyle={{fontFamily: 'alpha-regular', color: 'lightgray'}}
                            placeholder='dd.mm.yyyy'
                        />
                    </TouchableOpacity>

                    <SwitchSelector
                        style={{
                            marginHorizontal: 10,
                            marginVertical: 20,
                            backgroundColor: 'transparent',
                            alignSelf: "center"
                        }}
                        initial={this.state.gender === 'm' ? 0 : 1}
                        onPress={(value) => this._updateInputControl('gender', value)}
                        textColor={'black'} //'#7a44cf'
                        textStyle={{fontFamily: 'alpha-regular'}}
                        selectedTextStyle={{fontFamily: 'alpha-regular'}}
                        selectedColor={ColorDefinitions.secondaryText.shade0}
                        buttonColor={ColorDefinitions.colorAccent}
                        borderColor={ColorDefinitions.primaryText.shade0}
                        backgroundColor={'rgba(255,255,255,.4)'}
                        hasPadding
                        options={i18n.t('lbl_genders')}
                    />
                </View>

                <View style={styles.signUpSection}>
                    <Text style={styles.sectionHeading}>{i18n.t('lbl_contact_information')}</Text>
                    <Input
                        rightIcon={<MaterialCommunityIcons name={'email'} size={20} color={ColorDefinitions.primaryText.shade0}/>}
                        value={this.state.email}
                        keyboardType='email-address'
                        containerStyle={{ paddingTop: 5}}
                        inputContainerStyle={{borderColor: ColorDefinitions.primaryText.shade0, borderBottomWidth:1}}
                        inputStyle={{fontFamily: 'alpha-regular'}}
                        label={i18n.t('lbl_email_address')}
                        labelStyle={{fontFamily: 'alpha-regular', fontWeight: TypoDefinitions.primaryText, fontSize: TypoDefinitions.xSmallFont, color: ColorDefinitions.primaryText.shade0}}
                        placeholder='jogndoe@email.com'
                        onChangeText={val => this._updateInputControl('email', val)}
                    />

                    <Input
                        rightIcon={<MaterialCommunityIcons name={'cellphone'} size={20} color={ColorDefinitions.primaryText.shade0}/>}
                        value={this.state.phone}
                        keyboardType='phone-pad'
                        containerStyle={{ paddingTop: 5}}
                        inputContainerStyle={{borderColor: ColorDefinitions.primaryText.shade0, borderBottomWidth:1}}
                        inputStyle={{fontFamily: 'alpha-regular'}}
                        label={i18n.t('lbl_phone')}
                        labelStyle={{fontFamily: 'alpha-regular', fontWeight: TypoDefinitions.primaryText, fontSize: TypoDefinitions.xSmallFont, color: ColorDefinitions.primaryText.shade0}}
                        placeholder='+## #### ### ###'
                        onChangeText={val => this._updateInputControl('phone', val)}
                    />
                </View>

                <View style={styles.signUpSection}>
                    <Text style={styles.sectionHeading}>{i18n.t('lbl_security_information')}</Text>
                    <Input
                        autoCapitalize='none'
                        value={this.state.password}
                        containerStyle={{ paddingTop: 5}}
                        inputContainerStyle={{borderColor: ColorDefinitions.primaryText.shade0, borderBottomWidth:1}}
                        secureTextEntry={true}
                        label={i18n.t('lbl_password')}
                        labelStyle={{fontFamily: 'alpha-regular', fontWeight: TypoDefinitions.primaryText, fontSize: TypoDefinitions.xSmallFont, color: ColorDefinitions.primaryText.shade0}}
                        placeholder={i18n.t('lbl_password')}
                        onChangeText={val => this._updateInputControl('password', val)}
                    />

                    <Input
                        autoCapitalize='none'
                        value={this.state.passwordConfirm}
                        containerStyle={{ paddingTop: 5}}
                        inputContainerStyle={{borderColor: ColorDefinitions.primaryText.shade0, borderBottomWidth:1}}
                        secureTextEntry={true}
                        label={i18n.t('lbl_password_confirm')}
                        labelStyle={{fontFamily: 'alpha-regular', fontWeight: TypoDefinitions.primaryText, fontSize: TypoDefinitions.xSmallFont, color: ColorDefinitions.primaryText.shade0}}
                        placeholder={i18n.t('lbl_password_confirm')}
                        onChangeText={val => this._updateInputControl('passwordConfirm', val)}
                    />

                    <View style={{marginHorizontal:10, marginVertical: 10}}>
                        <View style={{flexDirection: 'row', justifyContent: 'space-between', marginVertical: 5}}>
                            <Text style={{paddingVertical: 2, fontFamily: 'alpha-regular', color: ColorDefinitions.primaryText.shade0}}>
                                {i18n.t('lbl_agree_tc')}
                            </Text>
                            <Switch
                                value={this.state.agreeTC}
                                onValueChange = {(val) =>{this._updateInputControl('agreeTC', val)}}/>
                        </View>

                        <View style={{flexDirection: 'row', justifyContent: 'space-between', marginVertical: 5}}>
                            <Text style={{paddingVertical: 2, fontFamily: 'alpha-regular', color: ColorDefinitions.primaryText.shade0}}>
                                {i18n.t('lbl_info_correct')}
                            </Text>
                            <Switch
                                value={this.state.confirmCorrectness}
                                onValueChange = {(val) =>{this._updateInputControl('confirmCorrectness', val)}}/>
                        </View>
                    </View>

                    <Button
                        onPress={()=>this._attemptSignUp()}
                        containerStyle={{margin: 10}}
                        buttonStyle={{backgroundColor: ColorDefinitions.primaryText.shade0, borderRadius: 0}}
                        titleStyle={{textTransform: 'capitalize', fontFamily: 'alpha-regular', marginVertical:5, color: ColorDefinitions.secondaryText.shade0}}
                        title={i18n.t('btn_sign_up')}
                        raised
                        disabled = {
                            !this.state.membershipNumber||
                            !this.state.password||
                            !this.state.givenNames||
                            !this.state.surname||
                            !this.state.phone||
                            !this.state.dateOfBirth||
                            !this.state.passwordConfirm||
                            !this.state.agreeTC||
                            !this.state.confirmCorrectness||
                            (this.state.password !== this.state.passwordConfirm)
                        }/>
                </View>
            </ScrollView>
        )
    }

    _updateInputControl = (inputKey, inputValue) => {
        this.setState({
            ...this.state,
            [inputKey] : inputValue
        });
    };

    _attemptSignUp = async () => {
        const {navigation} = this.props;
        const title = this.state.gender === "m" ? 'Mrs' : 'Mrs'
        const names = this.state.givenNames.split(' ')
        const surname = this.state.surname
        const gender = this.state.gender
        const dob = this.state.gender
        const membershipNumber = this.state.membershipNumber
        const email = this.state.email
        const newUser = {
            id:Math.floor(Math.random() * 100),
            name:{"title": title,"first":names[0],"middle":"","last": surname},
            particulars:{"dob":dob,"gender":gender,"picture":{"large":"https://robohash.org/blanditiistotamqui.jpg?size=50x50&set=set1","medium":"https://robohash.org/harumdolordolores.jpg?size=50x50&set=set1","thumbnail":"https://robohash.org/etmollitianesciunt.bmp?size=50x50&set=set1"}},
            contacts:{"email":email,"phone":"(913) 8719202","cell":"+1 202 216 6439","location":{"street":"59 Norway Maple Pass","city":"Shawnee Mission","province_state":"Kansas","country":"United States","postcode":"66225"}},
            login:{"uuid":"a901d8fa-5405-4c1d-b224-8aa21d6fd26e","membership_number":membershipNumber,"password":"secret","md5":"5fccbfd762d2d4e5ce087063a8757873"},
            nationality:"US",
            payment_methods:[{"type":"mastercard","card_number":"3583056800565385","expiry_date":"05.11.2020","holder_name":"SAMUEL TADIWA KUZAKWAWO"}]

        }
        await this.props.attemptSignUp(newUser);

        navigation.navigate('Main');
    };
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: ColorDefinitions.mainBackground
    },

    signUpSection:{
        marginBottom: 10
    },

    sectionHeading:{
        backgroundColor: ColorDefinitions.primaryText.shade0,
        fontFamily: 'alpha-regular',
        fontSize: TypoDefinitions.xxBigFont,
        paddingVertical: 15,
        paddingHorizontal: 10,
        color: ColorDefinitions.secondaryText.shade0
    }
});

const mapStateToProps = (state) => {
    return {

    }
};

const matchDispatchToProps = dispatch => {
    return {
        attemptSignUp: (newUser) => dispatch(attemptSignUp(newUser))
    }
};

export default connect(mapStateToProps, matchDispatchToProps)(SignUpScreen);
