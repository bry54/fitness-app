import React, {Component} from 'react';
import {BackHandler, ImageBackground, StyleSheet, Text, View} from 'react-native';
import {connect} from "react-redux";
import images from "../../assets/images";
import ColorDefinitions from "../../constants/ColorDefinitions";
import Ripple from "react-native-material-ripple";
import {attemptSignIn, updateGlobalControls} from "../../store/utilities/actionsCollection";
import i18n from "../../localizations/i18n";
import SwitchSelector from "react-native-switch-selector";

class RegistrationOptions extends Component {

    constructor(props) {
        super(props);
        this._backHandler = this.backAction.bind(this);
    }

    componentDidMount() {
        this._unsubscribe = this.props.navigation.addListener('focus', () => {
            this._updateInputControl('exitApp',false);
            BackHandler.addEventListener("hardwareBackPress", this._backHandler);
        });
    }

    componentWillUnmount() {
        BackHandler.removeEventListener( "hardwareBackPress", this._backHandler);
        this._unsubscribe();
    }

    backAction = () => {
        if (this.props.navigation.isFocused()) {
            BackHandler.exitApp();
            return true;
        }
    };

    render () {
        const languages = i18n.t('languages');
        return (
            <ImageBackground source={ images.screens._02 } style={styles.container}>

                <View style={styles.controlsContainer}>
                    <View style={{marginVertical: 10}}>
                        <SwitchSelector
                            style={{marginVertical: 10, backgroundColor: 'transparent', width: 200, alignSelf: "center"}}
                            initial={0}
                            onPress={(value) => this._updateInputControl('language', value)}
                            textColor={'black'} //'#7a44cf'
                            selectedColor={ColorDefinitions.secondaryText.shade0}
                            buttonColor={ColorDefinitions.colorAccent}
                            borderColor={'transparent'}
                            backgroundColor={'rgba(255,255,255,.4)'}
                            hasPadding
                            options={languages}
                        />
                    </View>

                    <View>
                        <Text style={styles.catchPhrase}>{i18n.t('app_name')}</Text>
                    </View>

                    <View style={styles.optionContainer}>
                        <View style={styles.rippledButton}>
                            <Ripple rippleOpacity={.8} onPress={()=>this._navToCreateAccount()}>
                                <Text style={styles.buttonTitle}>
                                    {i18n.t('title_sign_up')}
                                </Text>
                            </Ripple>
                        </View>

                        <View style={styles.rippledButton}>
                            <Ripple rippleOpacity={.8} onPress={()=>this._navToSignIn()}>
                                <Text style={styles.buttonTitle}>
                                    {i18n.t('title_sign_in')}
                                </Text>
                            </Ripple>
                        </View>

                        <View style={styles.rippledButton}>
                            <Ripple rippleOpacity={.8} onPress={()=>this._navToHome()}>
                                <Text style={styles.buttonTitle}>
                                    {i18n.t('title_skip_registration')}
                                </Text>
                            </Ripple>
                        </View>

                        <View style={styles.rippledButton}>
                            <Ripple rippleOpacity={.8} onPress={()=>this._navWithDemoLogin()}>
                                <Text style={styles.buttonTitle}>
                                    {i18n.t('title_demo_login')}
                                </Text>
                            </Ripple>
                        </View>
                    </View>
                </View>
            </ImageBackground>
        )
    }

    _updateInputControl = (inputKey, inputValue) => {
        this.props.updateGlobalControl({key:inputKey, value: inputValue});
    };

    _navToCreateAccount = () =>{
        const {navigation} = this.props;
        navigation.navigate('Auth', { screen: 'SignUp'});
    };

    _navToSignIn = () =>{
        const {navigation} = this.props;
        navigation.navigate('Auth', { screen: 'SignIn'});
    };

    _navWithDemoLogin = async () =>{
        const {navigation} = this.props;
        await this.props.attemptSignIn();

        navigation.navigate('Main', { screen: 'Home'});
    };

    _navToHome = async () =>{
        const {navigation} = this.props;
        this.props.updateGlobalControl({key:'useAsGuest', value: true});

        navigation.navigate('Main', { screen: 'Home'});
    };
}

const styles = StyleSheet.create({
    container: {
        height: '100%'
    },

    controlsContainer: {
        width: '100%',
        height: '100%',
        padding: 20,
        marginTop: 50
    },

    optionContainer: {
        flexDirection: 'column',
        justifyContent: 'flex-end'
    },

    rippledButton:{
        marginVertical: 10,
        borderColor: ColorDefinitions.primaryText.shade0,
        borderWidth: .5
    },

    buttonTitle:{
        color: ColorDefinitions.primaryText.shade0,
        fontFamily: 'alpha-regular',
        fontSize: 20,
        paddingVertical: 11,
        textAlign: 'center'
    },

    catchPhrase:{
        borderRadius: 20,
        backgroundColor: 'rgba(255,255,255,.4)',
        marginVertical: 70,
        color: ColorDefinitions.primaryText.shade0,
        fontFamily: 'alpha-regular',
        textAlign: 'center',
        fontSize: 36
    },

    pickerHolder: {
        borderBottomColor: ColorDefinitions.primaryText.shade0,
        borderBottomWidth: 1,
        overflow: 'hidden',
        marginBottom: 20
    },
});

const mapStateToProps = (state) => {
    return {
        language: state.globalReducer.language,
    }
};

const matchDispatchToProps = dispatch => {
    return {
        updateGlobalControl: (controlObj) => dispatch(updateGlobalControls(controlObj)),
        attemptSignIn: () => dispatch(attemptSignIn(null, true))
    }
};

export default connect(mapStateToProps, matchDispatchToProps)(RegistrationOptions);
