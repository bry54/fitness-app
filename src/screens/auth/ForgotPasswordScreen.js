import React, {Component} from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import {connect} from "react-redux";
import {Button, Input} from "react-native-elements";
import TypoDefinitions from "../../constants/TypoDefinitions";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from "moment";
import {Appearance} from "react-native-appearance";
import ColorDefinitions from "../../constants/ColorDefinitions";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import i18n from "../../localizations/i18n";

class ForgotPasswordScreen extends Component {
    state={
        dobPickerVisible: false,
        email: '',
        dateOfBirth: new Date(),
    };

    render () {
        const colorScheme = Appearance.getColorScheme();
        return (
            <View style={styles.container}>
                <DateTimePickerModal
                    isDarkModeEnabled={colorScheme === 'dark'}
                    isVisible={this.state.dobPickerVisible}
                    mode="date"
                    onConfirm={async (date) => {
                        await this._updateInputControl('dateOfBirth', date);
                        this._updateInputControl('dobPickerVisible', false)
                    }}
                    onCancel={() => this._updateInputControl('dobPickerVisible', false)}
                />

                <Input
                    rightIcon={<MaterialCommunityIcons name={'email'} size={20} color={ColorDefinitions.primaryText.shade0}/>}
                    autoCapitalize='none'
                    value={this.state.email}
                    containerStyle={{ paddingTop: 10}}
                    inputContainerStyle={{borderColor: ColorDefinitions.primaryText.shade0, borderBottomWidth:2}}
                    label={i18n.t('lbl_email_address')}
                    labelStyle={{fontFamily: 'alpha-regular', fontWeight: TypoDefinitions.primaryText, fontSize: TypoDefinitions.xSmallFont, color: ColorDefinitions.secondaryText.shade0}}
                    inputStyle={{fontFamily: 'alpha-regular'}}
                    placeholder='johndoe@email.com'
                    onChangeText={val => this._updateInputControl('email', val)}
                />

                <TouchableOpacity
                    activeOpacity={.9}
                    onPress={() => {this._updateInputControl('dobPickerVisible', true)}}>
                    <Input
                        rightIcon={<MaterialCommunityIcons name={'calendar'} size={20} color={ColorDefinitions.primaryText.shade0}/>}
                        editable={false}
                        value={ moment(this.state.dateOfBirth).format('DD.MM.YYYY') }
                        containerStyle={{ paddingTop: 30}}
                        inputContainerStyle={{borderColor: ColorDefinitions.primaryText.shade0, borderBottomWidth:2}}
                        label={i18n.t('lbl_date_of_birth')}
                        labelStyle={{fontFamily: 'alpha-regular', fontWeight: TypoDefinitions.primaryText, fontSize: TypoDefinitions.xSmallFont, color: ColorDefinitions.secondaryText.shade0}}
                        inputStyle={{fontFamily: 'alpha-regular'}}
                        placeholder='dd.mm.yyyy'
                    />
                </TouchableOpacity>

                <View style={{flexDirection: 'row', marginHorizontal: 10, marginVertical: 40, justifyContent: 'space-between'}}>
                    <Button
                        onPress={() => { this.props.navigation.goBack()}}
                        containerStyle={{width: '45%'}}
                        buttonStyle={{backgroundColor: ColorDefinitions.primaryBackground.shade0}}
                        titleStyle={{ textTransform: 'capitalize', fontFamily: 'alpha-regular', marginVertical:5}}
                        title={i18n.t('btn_submit')}
                        raised/>

                    <Button
                        onPress={() => { this.props.navigation.goBack()}}
                        containerStyle={{width: '45%'}}
                        buttonStyle={{backgroundColor: 'red'}}
                        titleStyle={{textTransform: 'capitalize', fontFamily: 'alpha-regular', marginVertical:5}}
                        title={i18n.t('btn_cancel')}
                        raised/>
                </View>
            </View>
        )
    }

    _updateInputControl = (inputKey, inputValue) => {
        this.setState({
            ...this.state,
            [inputKey] : inputValue
        });
    };
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: ColorDefinitions.mainBackground,
        flex: 1,
        paddingHorizontal: 10,
        paddingVertical: 10,
    },

    pickerHolder: {
        borderBottomColor: ColorDefinitions.primaryText.shade0,
        borderBottomWidth: 2,
        overflow: 'hidden'
    },

    idHolder:{
        paddingTop: 20,
        paddingBottom: 10,
    },

    passwordHolder:{

    },

    actionsContainer:{
        marginHorizontal: 10,
        marginTop: 40,
    },

    optionButton:{
        marginVertical: 10,
    },
});

const mapStateToProps = (state) => {
    return {

    }
};

const matchDispatchToProps = dispatch => {
    return {

    }
};

export default connect(mapStateToProps, matchDispatchToProps)(ForgotPasswordScreen);
