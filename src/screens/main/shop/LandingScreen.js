import React, {Component} from 'react';
import {FlatList, ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import ColorDefinitions from "../../../constants/ColorDefinitions";
import {connect} from "react-redux";
import i18n from "../../../localizations/i18n";
import {Avatar, Divider, ListItem, PricingCard} from "react-native-elements";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import moment from "moment";

class LandingScreen extends Component {

    render () {
        const user = this.props.user
        return (
            <View style={styles.container}>
                <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingTop: 10, paddingBottom: 20, paddingHorizontal: 10, backgroundColor: ColorDefinitions.primaryBackground.shade0, borderBottomLeftRadius: 0, borderBottomRightRadius: 0}}>
                    {user && <View>
                        <Text style={{fontSize: 24, color: ColorDefinitions.primaryText.shade0, fontFamily: 'alpha-regular'}}>{i18n.t('lbl_hello')}, {this.props.user.name.first} </Text>
                        <Text style={{fontSize: 16, color: 'silver', fontFamily: 'alpha-regular'}}>Membership ends on {moment().add('100','days').format('DD/MM/YY')}</Text>
                    </View>
                    }

                    <View style={{flexDirection: 'row'}}>
                        <View style={{
                            paddingHorizontal: 6,
                            borderLeftWidth: 1,
                            borderLeftColor: ColorDefinitions.secondaryBackground.shade0}}>
                        </View>
                        <View style={{alignSelf: 'center'}}>
                            {user ?
                                <View>
                                    <Text style={{fontSize: 15, color: ColorDefinitions.primaryText.shade0, fontFamily: 'alpha-regular'}}>{i18n.t('lbl_days_remaining')}</Text>
                                    <Text style={{fontSize: 12, color: 'yellow', fontFamily: 'numeric-regular', paddingVertical: 5}}>100 {i18n.t('lbl_days')}</Text>
                                </View> :
                                <View>
                                    <Text style={{fontSize: 15, color: ColorDefinitions.primaryText.shade0, fontFamily: 'alpha-regular'}}>Have a look at out flexible gym memberships</Text>
                                    <TouchableOpacity onPress={() => this._navToAuth()} style={{ margin: 0, borderWidth: 0, marginTop: 5}}>
                                        <Text style={styles.btnText}>{'Login if you are member'}</Text>
                                    </TouchableOpacity>
                                </View>
                            }
                        </View>
                    </View>
                </View>

                <ScrollView contentContainerStyle={{justifyContent: 'center'}}>
                    <View style={{paddingVertical: 5, alignSelf: 'center'}}>
                        <FlatList
                            showsHorizontalScrollIndicator={false}
                            horizontal={true}
                            keyExtractor={(item, index)=>index.toString()}
                            data={[0,1,2]}
                            renderItem={this._renderPricingCard}
                        />
                    </View>
                </ScrollView>

            </View>
        );
    }

    _renderPricingCard = ({ item }) =>{
        const memberships = [
            {
                title: 'Everyday Beast',
                price: "TRY "+ (Math.floor(Math.random() * 100) + 80),
                info: ['1 Person', '1 Month', 'All Day Access']
            },{
                title: 'The Night Owl',
                price: "TRY "+ (Math.floor(Math.random() * 100) + 80),
                info: ['1 Person', '1 Month', '17:30 - 23:00 Access']
            },{
                title: 'Power Group',
                price: "TRY "+ (Math.floor(Math.random() * 200) + 100),
                info: ['Minimum 2 people', '1 Month', '08:00 - 17:00 Access']
            }
        ]
        const data = memberships[item]
        return (
            <PricingCard
                containerStyle={{padding: 10, borderRadius: 10, borderColor: 'transparent', backgroundColor: 'rgba(0,0,0, .1)'}}
                titleStyle={{fontFamily: 'alpha-regular', fontWeight: '400', fontSize: 26}}
                pricingStyle={{fontFamily: 'numeric-regular', fontWeight: '400', fontSize: 22}}
                infoStyle={{fontFamily: 'alpha-regular', fontWeight: '400', fontSize: 18, color: 'silver'}}
                color={ColorDefinitions.primaryText.shade0}
                title={data.title}
                price={data.price+'.00'}
                info={data.info}
                button={{
                    buttonStyle: {padding: 5},
                    title: ' SEE MORE',
                    titleStyle: {fontFamily: 'alpha-regular', fontWeight: '400', fontSize: 16},
                    icon: {name:'shopping', type: 'material-community', color: 'white', size: 16}
                }}
            />
        )
    }

    _navToAuth = () =>{
        const {navigation} = this.props;
        navigation.navigate('Auth')
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: ColorDefinitions.mainBackground,
    },

    btnText: {
        fontWeight: 'normal',
        color: ColorDefinitions.colorAccent,
        fontSize: 18,
        backgroundColor: 'rgba(255, 255, 255, 0.1)',
        textAlign: 'center',
        fontFamily: 'alpha-regular',
        borderRadius: 50,
        padding:5
    },

});

const mapStateToProps = (state) => {
    return {
        user: state.authReducer.loggedInUser,
        defaultProgram: state.globalReducer.defaultProgram,
    }
};

const matchDispatchToProps = dispatch => {
    return {

    }
};

export default connect(mapStateToProps, matchDispatchToProps)(LandingScreen);
