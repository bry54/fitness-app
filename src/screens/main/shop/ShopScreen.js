import React, {Component} from 'react';
import {ActivityIndicator, FlatList, ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import ColorDefinitions from "../../../constants/ColorDefinitions";
import {connect} from "react-redux";
import i18n from "../../../localizations/i18n";
import {Avatar, Divider, Image, ListItem, PricingCard} from "react-native-elements";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import moment from "moment";
import LayoutDefinitions from "../../../constants/LayoutDefinitions";

class ShopScreen extends Component {

    render () {
        return (
            <View style={styles.container}>
                <FlatList style={{margin:0}}
                          showsVerticalScrollIndicator={false}
                          data={[0,1,2,3,4,5]}
                          numColumns={2}
                          keyExtractor={(item, index) => item.toString() }
                          renderItem={this._renderProductCard} />

            </View>
        );
    }

    _renderProductCard = ({ item }) =>{
        const letters = '0123456789ABCDEF';
        let color='';
        for (let i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        const products = [
            {
                title: 'Su / Water',
                price: "TRY "+ (Math.floor(Math.random() * 3) + 1),
                image: 'https://via.placeholder.com/150/'+color
            },{
                title: 'Protein Takviyesi / Shake',
                price: "TRY "+ (Math.floor(Math.random() * 100) + 80),
                image: 'https://via.placeholder.com/150/'+color
            },{
                title: 'Havlu / Towel',
                price: "TRY "+ (Math.floor(Math.random() * 200) + 100),
                image: 'https://via.placeholder.com/150/'+color
            },{
                title: 'Eldiven / Gloves',
                price: "TRY "+ (Math.floor(Math.random() * 200) + 100),
                image: 'https://via.placeholder.com/150/'+color
            },{
                title: 'kütle Kazanan / Mass Gainer',
                price: "TRY "+ (Math.floor(Math.random() * 200) + 100),
                image: 'https://via.placeholder.com/150/'+color
            },{
                title: 'Ayakkabi / Tracking Shoes',
                price: "TRY "+ (Math.floor(Math.random() * 200) + 100),
                image: 'https://via.placeholder.com/150/'+color
            }
        ]
        const data = products[item]
        return (
            <View style={{width: (LayoutDefinitions.window.width/2)-20, margin: 10, padding: 0, borderBottomLeftRadius: 10, borderBottomRightRadius: 10, borderColor: 'transparent', backgroundColor: 'rgba(0,0,0, .1)'}}>
                <Image
                    source={{ uri: data.image }}
                    style={{ width: '100%', height: 160 }}
                    PlaceholderContent={<ActivityIndicator />}
                />
                <View style={{flexDirection: 'column', justifyContent: 'space-between', padding: 5}}>
                    <Text style={{color: ColorDefinitions.primaryText.shade0, fontFamily: 'alpha-regular', fontSize: 14}}>{data.title}</Text>
                    <Text style={{color: 'silver', fontFamily: 'numeric-regular', fontSize: 12, textAlign: 'right'}}>{data.price}</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: ColorDefinitions.mainBackground,
        alignItems: 'center'
    },

});

const mapStateToProps = (state) => {
    return {
        user: state.authReducer.loggedInUser,
        defaultProgram: state.globalReducer.defaultProgram,
    }
};

const matchDispatchToProps = dispatch => {
    return {

    }
};

export default connect(mapStateToProps, matchDispatchToProps)(ShopScreen);
