import React, {Component} from 'react';
import {Linking, ScrollView, StyleSheet, Text, View} from 'react-native';
import { WebView } from 'react-native-webview';
import ColorDefinitions from "../../../constants/ColorDefinitions";
import {connect} from "react-redux";
import {exercises, muscles} from "../../../utilities/api/workouts";
import {Image, Tile} from "react-native-elements";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import Ripple from "react-native-material-ripple";
import HTML from "react-native-render-html";
import images from '../../../assets/images'

const MuscleItem = (props) =>{
    const item = props.item
    return (
        <View style={{backgroundColor: 'rgba(52, 52, 52, 0.5)', marginRight: 5, borderRadius: 5, padding: 3, alignItems: 'center', elevation: 0}}>
            <Text style={{color: 'yellow'}}> {item.title} </Text>
        </View>
    )
}

const SectionTitle = (props) =>{
    return (
        <Text style={{paddingVertical: 10, fontSize: 20, fontFamily: 'alpha-regular', textDecorationLine: 'underline'}}>{props.title}</Text>
    )
}
class ExerciseDescriptionScreen extends Component {

    render () {
        const {route, navigation} = this.props;
        const exercise = route.params.exercise;
        const muscleList = this._getMuscles();
        const user = this.props.user;
        const gender = user ? user.particulars.gender.toLowerCase() : 'm';

        return (
            <ScrollView style={styles.container}>
                <View style={{marginBottom: 0, alignItems: 'center'}}>
                    <Tile
                        imageSrc={ muscleList[0].img[gender] }
                        icon={{
                            name: 'play-circle-outline',
                            type: 'material-community',
                            color: 'lime',
                            size: 40,
                            onPress: async ()=> {
                                await Linking.openURL(exercise.video);
                            }
                        }}
                        featured
                    />
                </View>

                <View style={{paddingHorizontal: 10, paddingBottom: 10}}>
                    <SectionTitle title={'Target Muscles'}/>
                    <View style={{flexDirection: 'row'}}>
                        {
                            muscleList.map((muscle, index) => <MuscleItem key={index} item={muscle} />)
                        }
                    </View>
                </View>

                <View style={{paddingHorizontal: 10}}>
                    <SectionTitle title={'Instructions'}/>
                    <HTML tagsStyles={{ol: {fontFamily: 'alpha-regular', fontSize: 16}}} html={exercise.instructions} />
                </View>
            </ScrollView>
        );
    }

    _getMuscles = () =>{
        const exercise = this.props.route.params.exercise;
        let targetMuscles = [];
        muscles.forEach(muscle => {
            exercise.targetMuscles.forEach(tm =>{
                if (tm === muscle.id)
                    targetMuscles.push(muscle)
            })
        })

        return targetMuscles;
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 0,
        backgroundColor: ColorDefinitions.mainBackground,
    },
});

const mapStateToProps = (state) => {
    return {
        user: state.authReducer.loggedInUser
    }
};

const matchDispatchToProps = dispatch => {
    return {

    }
};

export default connect(mapStateToProps, matchDispatchToProps)(ExerciseDescriptionScreen);
