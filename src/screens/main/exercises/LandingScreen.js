import React, {Component} from 'react';
import {StyleSheet, FlatList, View, ActivityIndicator, Text, ImageBackground, TouchableOpacity} from 'react-native';
import ColorDefinitions from "../../../constants/ColorDefinitions";
import {connect} from "react-redux";
import {muscles} from "../../../utilities/api/workouts";
import LayoutDefinitions from "../../../constants/LayoutDefinitions";

class LandingScreen extends Component {

    render () {
        const user = this.props.user;
        const gender = user ? user.particulars.gender.toLowerCase() : 'm';

        return (
            <View style={styles.container}>
                <FlatList
                    numColumns={2}
                    data={muscles.map(group => {
                        return {
                            ...group,
                            img: group.img[gender]
                        }
                    })}
                    renderItem={(item) => this.renderItem(item)}
                    keyExtractor={(item => JSON.stringify(item))}
                />
            </View>
        );
    }

    renderItem = ({item}) => {
        return(
            <ImageBackground source={item.img} style={styles.specialsContainer}>
                <TouchableOpacity onPress={() => this._loadExercises(item)} style={{ height: 235, margin: 0, borderWidth: 0, backgroundColor: 'rgba(31, 19, 78, 0.4)' }}>
                    <Text style={styles.title}>{item.title}</Text>
                </TouchableOpacity>
            </ImageBackground>
        )
    }

    _loadExercises = (item) =>{
        const {navigation} = this.props;
        navigation.navigate('MuscleExercises', {muscle: item})
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: ColorDefinitions.mainBackground,
    },

    specialsContainer: {
        width: LayoutDefinitions.window.width/2,
        height: 200,
        paddingHorizontal: 0,
    },

    title: {
        fontWeight: 'normal',
        color: ColorDefinitions.colorAccent,
        fontSize: 26,
        backgroundColor: 'rgba(255, 255, 255, 0.1)',
        textAlign: 'center',
        marginBottom: 16,
        fontFamily: 'alpha-regular',
        marginVertical: 100,
        marginHorizontal: 10,
        paddingHorizontal: 10,
        borderRadius: 50
    },
});

const mapStateToProps = (state) => {
    return {
        user: state.authReducer.loggedInUser
    }
};

const matchDispatchToProps = dispatch => {
    return {

    }
};

export default connect(mapStateToProps, matchDispatchToProps)(LandingScreen);
