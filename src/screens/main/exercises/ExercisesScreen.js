import React, {Component} from 'react';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import ColorDefinitions from "../../../constants/ColorDefinitions";
import {connect} from "react-redux";
import {exercises} from "../../../utilities/api/workouts";
import {ListItem} from "react-native-elements";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import Ripple from "react-native-material-ripple";

const SubtitleItem = (props) =>{
    const item = props.item
    return (
       <View style={{backgroundColor: 'rgba(52, 52, 52, 0.5)', borderRadius: 5, padding: 5, alignItems: 'center', elevation: 5}}>
           <Text style={{color: 'yellow', fontFamily: 'alpha-regular', fontSize: 16}}> {item.title} </Text>
           <Text style={{color: 'silver', fontFamily: 'numeric-regular', paddingTop:3, fontSize: 11}}> {item.value} </Text>
       </View>
    )
}
class ExercisesScreen extends Component {

    render () {
        const {route} = this.props;
        const muscleGroup = route.params.muscle;

        const list = exercises.filter(exercise => exercise.targetMuscles.includes(muscleGroup.id) )
        return (
            <ScrollView style={styles.container}>
                {list.map((item, i) => (
                    <ListItem
                        key={i}
                        title={item.title}
                        onPress={() => this._navToExerciseDescription(item)}
                        containerStyle={{marginHorizontal: 10, marginVertical: 5, backgroundColor: ColorDefinitions.primaryBackground.shade0, borderRadius: 10}}
                        titleStyle={{fontSize: 17,fontFamily: 'alpha-regular', color: ColorDefinitions.primaryText.shade0, paddingVertical: 5}}
                        subtitle={
                            <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop: 5}}>
                                <SubtitleItem item={{title: 'Sets', value: item.sets}}/>
                                <SubtitleItem item={{title: typeof item.reps === 'number' ? 'Reps' : 'Duration', value: item.reps}}/>
                                <SubtitleItem item={{title: 'Rest', value: item.rest}}/>
                            </View>
                        }
                        leftIcon={<MaterialCommunityIcons name={item.icon} color={ColorDefinitions.primaryText.shade0} size={25}/>}
                        subtitleStyle={{fontFamily: 'alpha-regular',color: ColorDefinitions.primaryText.shade0}}
                        chevron={false}/>

                ))}
            </ScrollView>
        );
    }

    _navToExerciseDescription = (exercise) => {
        const {navigation} = this.props;
        navigation.navigate('ExerciseDescription', {exercise: exercise})
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 10,
        backgroundColor: ColorDefinitions.mainBackground,
    },
});

const mapStateToProps = (state) => {
    return {

    }
};

const matchDispatchToProps = dispatch => {
    return {

    }
};

export default connect(mapStateToProps, matchDispatchToProps)(ExercisesScreen);
