import React, {Component} from 'react';
import {Dimensions, Linking, ScrollView, StyleSheet, Text, View} from 'react-native';
import { WebView } from 'react-native-webview';
import ColorDefinitions from "../../../constants/ColorDefinitions";
import {connect} from "react-redux";
import {meals, categories} from "../../../utilities/api/diets";
import {ButtonGroup, Divider, Image, Tile} from "react-native-elements";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import Ripple from "react-native-material-ripple";
import HTML from "react-native-render-html";
import images from '../../../assets/images'
import LayoutDefinitions from "../../../constants/LayoutDefinitions";

const SubtitleItem = (props) =>{
    const item = props.item
    return (
        <View style={{alignItems: 'center'}}>
            <MaterialCommunityIcons name={item.icon} size={14} />
            <Divider style={{padding: .1, marginVertical: 2, backgroundColor: 'yellow', width: '50%'}}/>
            <Text style={{fontFamily: 'numeric-regular', color: ColorDefinitions.secondaryText.shade0}}>{item.value}</Text>
        </View>
    )
}

const NutritionItem = (props) =>{
    const item = props.item
    return (
        <View>
            <Text style={{fontFamily: 'alpha-regular'}}>{item.title}</Text>
            <Text style={{fontFamily: 'numeric-regular'}}>{item.value}</Text>
        </View>
    )
}

class MealDescriptionScreen extends Component {

    state = {
        buttonGroupIndex: 0,
    };

    render () {
        const buttons = ['Nutrient Count', 'Directions'];
        const {route, navigation} = this.props;
        const {meal, image} = route.params;

        return (
            <ScrollView style={styles.container}>
                <View style={{marginBottom: 10}}>
                    <Image
                        source={image}
                        style={{ width: LayoutDefinitions.window.width, height: 200 }}/>

                    <View style={{paddingHorizontal: 10}}>
                        <Text style={{fontFamily: 'alpha-regular', fontSize: 24 , color: ColorDefinitions.colorAccent, paddingBottom: 5}}>{meal.title_en}</Text>
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                            <SubtitleItem item={{icon: 'clock', value: meal.prep_time}} />
                            <SubtitleItem item={{icon: 'fire', value: meal.calories_count}} />
                            <SubtitleItem item={{icon: 'room-service', value: meal.servings}} />
                        </View>
                    </View>
                </View>

                <View style={{elevation: 5, paddingHorizontal: 5, paddingVertical: 5, backgroundColor: ColorDefinitions.colorPrimary}}>
                    <ButtonGroup
                        innerBorderStyle={{color: 'transparent'}}
                        selectedButtonStyle={{backgroundColor: 'transparent', borderBottomWidth: 2, borderBottomColor: ColorDefinitions.primaryText.shade0}}
                        textStyle={{textTransform: 'uppercase', fontFamily: 'alpha-regular', fontSize: 15, color: ColorDefinitions.colorAccent}}
                        selectedTextStyle={{fontFamily: 'alpha-regular', fontSize: 15 , color: ColorDefinitions.primaryText.shade0}}
                        onPress={(index) => this._updateInputControl('buttonGroupIndex', index)}
                        selectedIndex={this.state.buttonGroupIndex}
                        buttons={buttons}
                        containerStyle={{height: 40, backgroundColor: ColorDefinitions.colorPrimary, borderWidth: 0, borderRadius: 0}}
                    />
                </View>

                <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={styles.container}>
                    { this.state.buttonGroupIndex === 0 ?
                        (
                            <View style={{flex: 1, justifyContent: 'space-between', paddingTop: 10}}>
                                <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingBottom: 10, paddingHorizontal: 10}}>
                                    <NutritionItem item={{title: 'Carbs', value: meal.carbs_count}} />
                                    <NutritionItem item={{title: 'Protein', value: meal.protein_count}} />
                                    <NutritionItem item={{title: 'Fat', value: meal.fat_count}} />
                                </View>
                                <HTML tagsStyles={{ul: {fontFamily: 'alpha-regular', fontSize: 16}}} html={meal.ingredients_en} />
                            </View>
                        ) : (
                            <View style={{flex: 1, justifyContent: 'space-between', paddingTop: 10}}>
                                <HTML tagsStyles={{ol: {fontFamily: 'alpha-regular', fontSize: 16}}} html={meal.directions_en} />
                            </View>
                        )
                    }
                </ScrollView>
            </ScrollView>
        );
    }

    _getMuscles = () =>{
        const exercise = this.props.route.params.exercise;
        let targetMuscles = [];
        muscles.forEach(muscle => {
            exercise.targetMuscles.forEach(tm =>{
                if (tm === muscle.id)
                    targetMuscles.push(muscle)
            })
        })

        return targetMuscles;
    }

    _updateInputControl = (inputKey, inputValue) => {
        this.setState({
            ...this.state,
            [inputKey] : inputValue
        });
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 0,
        backgroundColor: ColorDefinitions.mainBackground,
    },
});

const mapStateToProps = (state) => {
    return {

    }
};

const matchDispatchToProps = dispatch => {
    return {

    }
};

export default connect(mapStateToProps, matchDispatchToProps)(MealDescriptionScreen);
