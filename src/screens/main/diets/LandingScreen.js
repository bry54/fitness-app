import React, {Component} from 'react';
import {ImageBackground, ScrollView, StyleSheet, Text, View} from 'react-native';
import ColorDefinitions from "../../../constants/ColorDefinitions";
import {connect} from "react-redux";
import {categories, meals} from "../../../utilities/api/diets";
import {ListItem} from "react-native-elements";
import {MaterialCommunityIcons} from "@expo/vector-icons";

class LandingScreen extends Component {

    render () {
        const user = this.props.user;
        const gender = user ? user.particulars.gender.toLowerCase() : 'f';

        const genderCategories = categories.map(category =>{
            return {
                ...category,
                img: category.img[gender]
            }
        })
        return (
            <ScrollView style={styles.container}>
                {
                    genderCategories.map((category, index) => (
                        <ListItem
                            key={index}
                            onPress={()=>this._navToMealListings(category)}
                            containerStyle={{
                                marginHorizontal: 10,
                                marginVertical: 10,
                                backgroundColor: ColorDefinitions.primaryBackground.shade0,
                                borderRadius: 10
                            }}
                            leftAvatar={() =>
                                <ImageBackground source={category.img} style={{height: 100, width: 100}} imageStyle={{borderRadius: 10, backgroundColor: ColorDefinitions.colorPrimary}}>
                                    <View style={{borderRadius: 10, height: 100, backgroundColor: 'rgba(255, 255, 0, 0.2)'}}></View>
                                </ImageBackground>
                            }
                            title={category.title_en}
                            titleStyle={styles.title}
                            subtitle={category.description_en}
                            subtitleStyle={styles.description}
                            chevron={<MaterialCommunityIcons name={'chevron-right'} color={ColorDefinitions.primaryText.shade0} size={16} />}
                    />
                    ))
                }
            </ScrollView>
        );
    }

    _navToMealListings = (category) =>{
        const {navigation} = this.props;
        navigation.navigate('MealsListing', {dietCategory: category});
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: ColorDefinitions.mainBackground,
    },

    title: {
        fontWeight: 'normal',
        color: ColorDefinitions.colorAccent,
        fontSize: 22,
        textAlign: 'left',
        fontFamily: 'alpha-regular',
        marginVertical: 5,
        marginHorizontal: 0,
        paddingHorizontal: 0,
        borderRadius: 0
    },

    description: {
        fontWeight: 'normal',
        color: 'yellow',
        textAlign: 'left',
        fontFamily: 'alpha-regular',
        marginVertical: 0,
        marginHorizontal: 0,
        paddingHorizontal: 0,
        borderRadius: 0
    },
});

const mapStateToProps = (state) => {
    return {
        user: state.authReducer.loggedInUser,
    }
};

const matchDispatchToProps = dispatch => {
    return {

    }
};

export default connect(mapStateToProps, matchDispatchToProps)(LandingScreen);
