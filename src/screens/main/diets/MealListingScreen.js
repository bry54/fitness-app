import React, {Component} from 'react';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import ColorDefinitions from "../../../constants/ColorDefinitions";
import {connect} from "react-redux";
import {meals, categories} from "../../../utilities/api/diets";
import {ListItem} from "react-native-elements";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import Ripple from "react-native-material-ripple";

const SubtitleItem = (props) =>{
    const item = props.item
    return (
        <View style={{borderRadius: 5, padding: 5, alignItems: 'center', flexDirection: 'column'}}>
            <MaterialCommunityIcons name={item.icon} color = {ColorDefinitions.secondaryText.shade0} size={16}/>
            <Text style={{color: 'silver', fontFamily: 'numeric-regular'}}> {item.value} </Text>
        </View>
    )
}
class MealListingScreen extends Component {

    render () {
        const category = this.props.route.params.dietCategory;
        const list = this._getCategoryMeals()
        return (
            <ScrollView style={styles.container}>
                {list.map((item, i) => (
                    <ListItem
                        key={i}
                        title={item.title_en}
                        onPress={() => this._navToMealDescription(item)}
                        containerStyle={{marginHorizontal: 10, marginVertical: 5, backgroundColor: 'rgba(0,0,0, .1)', borderRadius: 10}}
                        titleStyle={{fontSize: 17,fontFamily: 'alpha-regular', color: ColorDefinitions.primaryText.shade0, paddingVertical: 0}}
                        subtitle={
                            <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop: 5}}>
                                <SubtitleItem item={{icon: 'clock', value: item.prep_time}}/>
                                <SubtitleItem item={{icon: 'fire', value: item.calories_count}}/>
                            </View>
                        }
                        leftAvatar={{
                            size: 'large',
                            avatarStyle: {borderRadius: 5},
                            rounded: false,
                            source: category.img,
                        }}
                        subtitleStyle={{fontFamily: 'alpha-regular',color: ColorDefinitions.primaryText.shade0}}
                        chevron={false}/>

                ))}
            </ScrollView>
        );
    }

    _getCategoryMeals = () => {
        const {route} = this.props;
        const category = route.params.dietCategory;
        return meals.filter(meal => meal.category === category.id)
    };

    _navToMealDescription = (meal) => {
        const {navigation, route} = this.props;
        const category = route.params.dietCategory;
        navigation.navigate('MealDescription', {meal: meal, image: category.img})
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 10,
        backgroundColor: ColorDefinitions.mainBackground,
    },
});

const mapStateToProps = (state) => {
    return {

    }
};

const matchDispatchToProps = dispatch => {
    return {

    }
};

export default connect(mapStateToProps, matchDispatchToProps)(MealListingScreen);
