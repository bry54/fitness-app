import React, {Component} from 'react';
import {ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import ColorDefinitions from "../../../constants/ColorDefinitions";
import {connect} from "react-redux";
import i18n from "../../../localizations/i18n";
import images from "../../../assets/images";
import Ripple from "react-native-material-ripple";
import {Avatar, Divider, ListItem} from "react-native-elements";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import moment from "moment";
import {exercises, programPlans, programs} from "../../../utilities/api/workouts";


const WeekdayExercise = (props) =>{
    const item = props.item


    return item ? (
        <View style={{padding: 5, flexDirection: 'row'}}>
            <MaterialCommunityIcons name={'dumbbell'} color={'silver'} style={{paddingTop: 5}}/>
            <Text style={{flexWrap: 'wrap', color: 'silver', fontFamily: 'numeric-regular', paddingTop:3, fontSize: 11}}> {item.title} </Text>
        </View>
    ) : null
}

class LandingScreen extends Component {

    render () {
        const programId = this.props.defaultProgram;
        const user = this.props.user;
        const homeMenu = [
            {icon: 'wunderlist', label: i18n.t('menu_workouts'), route: 'WorkoutsList', subTitle: i18n.t('workouts_subtitle'), isShown: !programId},
            {icon: 'dumbbell', label: i18n.t('menu_exercises'), route: 'Exercises', subTitle: i18n.t('exercises_subtitle'), isShown: true},
            {icon: 'food-fork-drink', label: i18n.t('menu_diets'), route: 'Diets', subTitle: i18n.t('diets_subtitle'), isShown: !programId},
            {icon: 'format-quote-open', label: i18n.t('menu_motivation'), route: 'Motivation', subTitle: i18n.t('motivation_subtitle'), isShown: true},
        ];
        const shownMenu = homeMenu.filter(item => item.isShown);
        const dataSet = this._getExercises(programId);
        const program = dataSet.program;
        const todayExercises = dataSet.exercises;

        return (
            <ScrollView style={styles.container}>
                <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingTop: 10, paddingBottom: 20, paddingHorizontal: 20, backgroundColor: ColorDefinitions.primaryBackground.shade0, borderBottomLeftRadius: 20, borderBottomRightRadius: 20}}>
                    <View>
                        <Text style={{fontSize: 24, color: ColorDefinitions.primaryText.shade0, fontFamily: 'alpha-regular'}}>{i18n.t('lbl_hello')}, </Text>
                        <Text style={{fontSize: 24, color: ColorDefinitions.primaryText.shade0, fontFamily: 'alpha-regular'}}>{user ? user.name.first : 'Guest'}</Text>
                    </View>

                    <View style={{flexDirection: 'row'}}>
                        <View style={{
                            paddingHorizontal: 5,
                            borderLeftWidth: 1,
                            borderLeftColor: ColorDefinitions.secondaryBackground.shade0}}>
                        </View>
                        <View style={{alignSelf: 'center'}}>
                            {user ?
                                <View>
                                    <Text style={{fontSize: 15, color: ColorDefinitions.primaryText.shade0, fontFamily: 'alpha-regular'}}>{i18n.t('lbl_days_remaining')}</Text>
                                    <Text style={{fontSize: 12, color: 'yellow', fontFamily: 'numeric-regular', paddingVertical: 5}}>100 {i18n.t('lbl_days')}</Text>
                                </View> :
                                <View>
                                    <Text style={{fontSize: 15, color: ColorDefinitions.primaryText.shade0, fontFamily: 'alpha-regular'}}>Login to see days remaining</Text>
                                    <TouchableOpacity onPress={() => this._navToAuth()} style={{ margin: 0, borderWidth: 0, marginTop: 5}}>
                                        <Text style={styles.btnText}>{'Login'}</Text>
                                    </TouchableOpacity>
                                </View>
                            }
                        </View>
                    </View>
                </View>

                <Avatar
                    containerStyle={{alignSelf: "center", marginTop: 10}}
                    size={'medium'}
                    rounded
                    source={images.logo}
                />

                {programId && <TouchableOpacity style={{
                    margin: 10,
                    backgroundColor: ColorDefinitions.primaryBackground.shade0,
                    borderRadius: 5,
                    padding: 10
                }} onPress={() => this._navToWorkout(moment().format('dddd'), todayExercises)}>
                    <Text style={{
                        fontSize: 18,
                        fontFamily: 'alpha-regular',
                        color: ColorDefinitions.primaryText.shade0,
                        paddingVertical: 10
                    }}>{program.title}</Text>
                    <Divider/>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                        <View style={styles.dateContainer}>
                            <Text style={styles.dd}>
                                {
                                    moment().format('DD')
                                }
                            </Text>
                            <View style={styles.monthYear}>
                                <Text style={styles.mm}>
                                    {
                                        moment().format('MMM')
                                    }
                                </Text>
                                <Text style={styles.yyyy}>
                                    {
                                        moment().format('YYYY')
                                    }
                                </Text>
                            </View>
                        </View>
                        {
                            todayExercises.length ? (
                                <View style={{flexDirection: 'column', marginTop: 5}}>
                                    {todayExercises.map(e => <WeekdayExercise key={JSON.stringify(e)} item={e}/>)}
                                </View>) : (
                                <View style={{flexDirection: 'column', marginTop: 5, justifyContent: 'center'}}>
                                    <Text style={{
                                        color: 'silver',
                                        fontFamily: 'numeric-regular',
                                        paddingTop: 3,
                                        fontSize: 14
                                    }}>
                                        {i18n.t('lbl_rest_day')}
                                    </Text>
                                </View>
                            )
                        }
                    </View>
                    <Divider/>
                    <Text style={{
                        fontSize: 14,
                        fontFamily: 'alpha-regular',
                        color: ColorDefinitions.primaryText.shade0,
                        paddingVertical: 5,
                        textAlign: 'right'
                    }}>
                        {'Tap to see full plan'}
                    </Text>
                </TouchableOpacity>}

                <View style={{marginVertical: 0}}>
                    {
                        shownMenu.map((item, index) => (
                            <Ripple
                                key={index}
                                onPress={() => this._openSection(item)}
                                rippleOpacity={.1}>
                                <ListItem
                                    title={item.label}
                                    containerStyle={{marginHorizontal: 10, marginVertical: 10, backgroundColor: ColorDefinitions.primaryBackground.shade0, borderRadius: 10}}
                                    titleStyle={{fontSize: 17,fontFamily: 'alpha-regular', color: ColorDefinitions.primaryText.shade0}}
                                    subtitle={item.subTitle}
                                    leftIcon={<MaterialCommunityIcons name={item.icon} color={ColorDefinitions.primaryText.shade0} size={25}/>}
                                    subtitleStyle={{fontFamily: 'alpha-regular',color: 'yellow'}}
                                    chevron={<MaterialCommunityIcons name={'chevron-right'} color={ColorDefinitions.primaryText.shade0} size={16} />}/>
                            </Ripple>
                        ))
                    }
                </View>
            </ScrollView>
        );
    }

    _navToWorkout = (dayName, exercises) =>{
        const {navigation} = this.props;
        navigation.navigate('Workout', {dayName: dayName, exerciseNames: exercises})
    }

    _navToAuth = () =>{
        const {navigation} = this.props;
        navigation.navigate('Auth')
    }

    _openSection = (item) => {
        const {navigation} = this.props;
        navigation.navigate(item.route)
    }

    _getExercises = (programId) =>{
        let data = [];
        let program = null;
        const programPlan = programPlans.find(p => p.program_id === programId)

        if (programPlan) {
            program = programs.find(p => p.id === programPlan.program_id);
            const plan = programPlan.plan;
            const todayPlan = plan.find(p => p.day === moment().isoWeekday())

            if (todayPlan) {
                data = todayPlan.exercises.map(exercise => {
                    return exercises.find(e => e.id === exercise)
                });
            }
        }

        return {
            program: program,
            exercises: data.length > 3 ? data.slice(0,3) : data
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: ColorDefinitions.mainBackground,
    },
    dateContainer:{
        flexDirection: 'row',
        paddingVertical: 5,
        alignSelf: 'center'
    },

    dd:{
        fontSize: 50,
        color: ColorDefinitions.primaryText.shade0,
        fontFamily: 'alpha-regular',
        paddingRight: 6
    },

    monthYear:{
        flexDirection: 'column',
        marginTop: 16
    },

    mm:{
        color: 'yellow', //ColorDefinitions.alternateText.shade0,
        fontFamily: 'alpha-regular',
    },

    yyyy:{
        color: 'yellow', //ColorDefinitions.alternateText.shade0,
        fontFamily: 'alpha-regular',
    },

    btnText: {
        fontWeight: 'normal',
        color: ColorDefinitions.colorAccent,
        fontSize: 18,
        backgroundColor: 'rgba(255, 255, 255, 0.1)',
        textAlign: 'center',
        fontFamily: 'alpha-regular',
        borderRadius: 50,
        padding:5
    },
});

const mapStateToProps = (state) => {
    return {
        user: state.authReducer.loggedInUser,
        defaultProgram: state.globalReducer.defaultProgram,
    }
};

const matchDispatchToProps = dispatch => {
    return {

    }
};

export default connect(mapStateToProps, matchDispatchToProps)(LandingScreen);
