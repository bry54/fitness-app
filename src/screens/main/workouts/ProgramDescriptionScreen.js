import React, {Component} from 'react';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import ColorDefinitions from "../../../constants/ColorDefinitions";
import {connect} from "react-redux";
import {exercises, programPlans} from "../../../utilities/api/workouts";
import {Divider, ListItem} from "react-native-elements";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import Ripple from "react-native-material-ripple";
import moment from "moment";

const WeekdayExercise = (props) =>{
    const item = props.item


    return item ? (
        <View style={{padding: 5, flexDirection: 'row'}}>
            <MaterialCommunityIcons name={'asterisk'} color={'silver'} style = {{paddingTop: 3}}/>
            <Text style={{color: 'silver', fontFamily: 'numeric-regular', paddingTop:3, fontSize: 11}}> {item.title} </Text>
        </View>
    ) : null
}

class ProgramDescriptionScreen extends Component {

    render () {
        const {route} = this.props;

        const list = this._getExercises()
        return (
            <ScrollView style={styles.container}>
                {list.map((item, i) => (
                    <ListItem
                        key={i}
                        title={item.dayName}
                        onPress={() => this._navToWorkoutScreen(item)}
                        containerStyle={{marginHorizontal: 10, marginVertical: 5, backgroundColor: ColorDefinitions.primaryBackground.shade0, borderRadius: 10, borderWidth:1, borderColor: item.dayName === moment().format('dddd') ? ColorDefinitions.primaryText.shade0 : 'silver'}}
                        titleStyle={{fontSize: 17,fontFamily: 'alpha-regular', color: item.dayName === moment().format('dddd') ? ColorDefinitions.primaryText.shade0 : 'silver', paddingVertical: 5}}
                        subtitle={
                            <View style={{flexDirection: 'column', marginTop: 5}}>
                                <Divider/>
                                {item.exerciseNames.map(e=> <WeekdayExercise key={JSON.stringify(e)} item={e}/>)}
                            </View>
                        }
                        subtitleStyle={{fontFamily: 'alpha-regular',color: ColorDefinitions.primaryText.shade0}}
                        chevron={false}/>

                ))}
            </ScrollView>
        );
    }

    _getExercises = () =>{
        const {navigation, route} = this.props;
        const program = route.params.program;
        const programPlan = programPlans.find(p => p.program_id === program.id)

        return programPlan.plan.map(pPlan =>{
            return {
                ...pPlan,
                dayName: moment().isoWeekday(pPlan.day).format('dddd'),
                exerciseNames: pPlan.exercises.map(exercise=>exercises.find(ex=>ex.id === exercise))
            }
        });
    }

    _navToWorkoutScreen = (program) => {
        const {navigation} = this.props;
        navigation.navigate('Workout', {program: program})
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: ColorDefinitions.mainBackground,
    },
});

const mapStateToProps = (state) => {
    return {

    }
};

const matchDispatchToProps = dispatch => {
    return {

    }
};

export default connect(mapStateToProps, matchDispatchToProps)(ProgramDescriptionScreen);
