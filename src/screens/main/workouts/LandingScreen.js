import React, {Component} from 'react';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import ColorDefinitions from "../../../constants/ColorDefinitions";
import {connect} from "react-redux";
import {Badge, ListItem} from "react-native-elements";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import {goals, levels, programs} from "../../../utilities/api/workouts";
import {updateGlobalControls} from "../../../store/actions/global";

class LandingScreen extends Component {

    render () {
        return (
            <ScrollView style={styles.container}>
                {programs.map((item, i) => {
                    const pLevel = levels.find(level => level.id === item.level)
                    const pGoal = goals.find(goal => goal.id === item.goal)
                    return (
                        <ListItem
                            key={i}
                            onPress={() => this._navToProgramDetail(item)}
                            title={item.title}
                            containerStyle={{marginHorizontal: 10, marginVertical: 5, backgroundColor: ColorDefinitions.primaryBackground.shade0, borderRadius: 10}}
                            titleStyle={{fontSize: 17,fontFamily: 'alpha-regular', color: ColorDefinitions.primaryText.shade0, paddingVertical: 5}}
                            subtitle={
                                <View style={{flexDirection: 'row', justifyContent: 'flex-start', marginTop: 5}}>
                                    <Badge
                                        containerStyle={{marginHorizontal: 10}}
                                        textStyle={{color: ColorDefinitions.secondaryText.shade0, fontFamily: 'alpha-regular'}}
                                        badgeStyle={{padding: 2, backgroundColor: 'silver', borderColor: 'transparent'}}
                                        value={pLevel.title}/>

                                    <Badge
                                        textStyle={{color: ColorDefinitions.secondaryText.shade0, fontFamily: 'alpha-regular'}}
                                        badgeStyle={{padding: 2, backgroundColor: 'silver', borderColor: 'transparent'}}
                                        value={pGoal.title}/>
                                </View>
                            }
                            leftIcon={<MaterialCommunityIcons name={item.icon} color={ColorDefinitions.primaryText.shade0} size={25}/>}
                            rightIcon={
                                <MaterialCommunityIcons
                                    name={'star'}
                                    color={this.props.defaultProgram === item.id ? 'yellow' : 'silver'}
                                    size={25}
                                    onPress={() => this._updateInputControl('defaultProgram', item.id)}/>
                            }
                            subtitleStyle={{fontFamily: 'alpha-regular',color: ColorDefinitions.primaryText.shade0}}
                            chevron={false}/>
                    )}
                )}
            </ScrollView>
        );
    }

    _navToProgramDetail = (program) =>{
        const {navigation} = this.props;
        navigation.navigate('ProgramDetail', {program: program})
    };

    _updateInputControl = (inputKey, inputValue) => {
        this.props.updateGlobalControl({key: inputKey, value: inputValue})
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: ColorDefinitions.mainBackground,
    },
});

const mapStateToProps = (state) => {
    return {
        defaultProgram: state.globalReducer.defaultProgram,
    }
};

const matchDispatchToProps = dispatch => {
    return {
        updateGlobalControl: (controlObj) => dispatch(updateGlobalControls(controlObj))
    }
};

export default connect(mapStateToProps, matchDispatchToProps)(LandingScreen);
