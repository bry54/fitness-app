import React, {Component} from 'react';
import {ScrollView, StyleSheet, Text, TouchableOpacity, View, LayoutAnimation, Modal} from 'react-native';
import ColorDefinitions from "../../../constants/ColorDefinitions";
import {connect} from "react-redux";
import {ButtonGroup, ListItem, Overlay} from "react-native-elements";
import {quotes, stories} from "../../../utilities/api/motivation";
import QuoteContainer from "../../../components/motivation/QuoteContainer";
import StoriesContainer from "../../../components/motivation/StoriesContainer";
import LayoutDefinitions from "../../../constants/LayoutDefinitions";
import moment from "moment";
import {MaterialCommunityIcons} from "@expo/vector-icons";

class LandingScreen extends Component {
    state = {
        buttonGroupIndex: 0,
        activeQuote: 0,
        activeStory: null
    };

    render () {
        const buttons = ['Quotes', 'Testimonies'];
        return (
            <View style={styles.container}>
                <View style={{elevation: 5, paddingHorizontal: 5, paddingVertical: 5, backgroundColor: ColorDefinitions.colorPrimary}}>
                    <ButtonGroup
                        innerBorderStyle={{color: 'transparent'}}
                        selectedButtonStyle={{backgroundColor: 'transparent', borderBottomWidth: 2, borderBottomColor: ColorDefinitions.colorAccent}}
                        textStyle={{textTransform: 'uppercase', fontFamily: 'alpha-regular', fontSize: 15, color: ColorDefinitions.primaryText.shade0}}
                        selectedTextStyle={{fontFamily: 'alpha-regular', fontSize: 15 , color: ColorDefinitions.primaryText.shade0}}
                        onPress={(index) => this._updateInputControl('buttonGroupIndex', index)}
                        selectedIndex={this.state.buttonGroupIndex}
                        buttons={buttons}
                        containerStyle={{height: 40, backgroundColor: ColorDefinitions.colorPrimary, borderWidth: 0, borderRadius: 0}}
                    />
                </View>
                <ScrollView style={{}} contentContainerStyle={{ flex: 1}}>
                    {
                        this.state.buttonGroupIndex === 0 ?
                            <QuoteContainer quote={quotes[this.state.activeQuote]} getNextQuote={this._getNextQuote}/> :
                            <StoriesContainer stories={stories} loadStory={this._loadStory}/>
                    }
                    <Overlay
                        overlayStyle={{height: LayoutDefinitions.window.height - 120}}
                        isVisible={!!this.state.activeStory}
                        onBackdropPress={() => this._loadStory(null)}>
                        <View>
                            {
                                this.state.activeStory && <View>
                                    <ListItem
                                        title={this.state.activeStory.author}
                                        containerStyle={{marginHorizontal: 0, marginVertical: 0}}
                                        titleStyle={{fontSize: 20,fontFamily: 'alpha-regular', color: ColorDefinitions.secondaryText.shade0, marginBottom: 10}}
                                        leftAvatar={{
                                            size: 'medium',
                                            title: this.state.activeStory.author.match(/\b(\w)/g).join('.'),
                                            titleStyle: {fontFamily: 'alpha-regular', color: ColorDefinitions.primaryText.shade0},
                                            rounded: false,
                                            overlayContainerStyle: {backgroundColor: ColorDefinitions.primaryBackground.shade0, borderRadius: 7}
                                        }}
                                        subtitle={ moment(this.state.activeStory.date).fromNow() }
                                        subtitleStyle={{fontFamily: 'alpha-regular',color: ColorDefinitions.secondaryText.shade0}}
                                    bottomDivider/>
                                    <Text style={{color: ColorDefinitions.secondaryText.shade0, marginTop: 10}}>{this.state.activeStory.story}</Text>
                                </View>
                            }
                        </View>
                    </Overlay>
                </ScrollView>
            </View>
        );
    }

    _updateInputControl = (inputKey, inputValue) => {
        this.setState({
            ...this.state,
            [inputKey] : inputValue
        });
    };

    _getNextQuote = () => {
        this._updateInputControl('activeQuote', Math.floor((Math.random() * (quotes.length))))
    }

    _loadStory = (story) => {
        this._updateInputControl('activeStory', story)
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: ColorDefinitions.mainBackground,
    },
});

const mapStateToProps = (state) => {
    return {

    }
};

const matchDispatchToProps = dispatch => {
    return {

    }
};

export default connect(mapStateToProps, matchDispatchToProps)(LandingScreen);
