import React, {Component} from 'react';
import {ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import ColorDefinitions from "../../../constants/ColorDefinitions";
import {connect} from "react-redux";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import i18n from "../../../localizations/i18n";
import {Input} from "react-native-elements";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import TypoDefinitions from "../../../constants/TypoDefinitions";
import moment from "moment";
import {Appearance} from "react-native-appearance";
import SwitchSelector from "react-native-switch-selector";
import {updateGlobalControls} from "../../../store/actions/global";

class LandingScreen extends Component {
    user = this.props.user
    state={
        email: this.props.user ? this.props.user.contacts.email : '',
        phone: this.props.user ? this.props.user.contacts.phone :'',
        givenNames: this.props.user ? this.props.user.name.first : '',
        surname: this.props.user ? this.props.user.name.last : '',
        dateOfBirth: this.props.user ? this.props.user.particulars.dob : new Date(),
        dobPickerVisible: false,
    };

    render () {
        const colorScheme = Appearance.getColorScheme();
        return (
            <ScrollView style={styles.container}>
                <DateTimePickerModal
                    isDarkModeEnabled={colorScheme === 'dark'}
                    isVisible={this.state.dobPickerVisible}
                    mode="date"
                    onConfirm={async (date) => {
                        await this._updateInputControl('dateOfBirth', date);
                        this._updateInputControl('dobPickerVisible', false)
                    }}
                    onCancel={() => this._updateInputControl('dobPickerVisible', false)}
                />

                <View style={styles.personalInformation}>
                    <Input
                        value={this.state.givenNames}
                        containerStyle={{ paddingTop: 10}}
                        inputContainerStyle={{borderColor: ColorDefinitions.primaryText.shade0, borderBottomWidth:1, paddingTop: 0, paddingBottom: 0}}
                        inputStyle={{fontFamily: 'alpha-regular'}}
                        label={i18n.t('lbl_first_name')}
                        labelStyle={{paddingBottom: 0, fontFamily: 'alpha-regular', fontWeight: TypoDefinitions.primaryText, fontSize: TypoDefinitions.xSmallFont, color: ColorDefinitions.primaryText.shade0}}
                        placeholder='John'
                        onChangeText={val => this._updateInputControl('givenNames', val)}
                    />

                    <Input
                        value={this.state.surname}
                        containerStyle={{ paddingTop: 0}}
                        inputContainerStyle={{borderColor: ColorDefinitions.primaryText.shade0, borderBottomWidth:1}}
                        inputStyle={{fontFamily: 'alpha-regular'}}
                        label={i18n.t('lbl_surname')}
                        labelStyle={{fontFamily: 'alpha-regular', fontWeight: TypoDefinitions.primaryText, fontSize: TypoDefinitions.xSmallFont, color: ColorDefinitions.colorAccent}}
                        placeholder='Doe'
                        onChangeText={val => this._updateInputControl('surname', val)}
                    />

                    <Input
                        value={this.state.phone}
                        containerStyle={{ paddingTop: 0}}
                        inputContainerStyle={{borderColor: ColorDefinitions.primaryText.shade0, borderBottomWidth:1}}
                        inputStyle={{fontFamily: 'alpha-regular'}}
                        label={i18n.t('lbl_phone')}
                        labelStyle={{fontFamily: 'alpha-regular', fontWeight: TypoDefinitions.primaryText, fontSize: TypoDefinitions.xSmallFont, color: ColorDefinitions.colorAccent}}
                        placeholder='+90533000 0 000'
                        onChangeText={val => this._updateInputControl('phone', val)}
                    />

                    <Input
                        value={this.state.email}
                        containerStyle={{ paddingTop: 0}}
                        inputContainerStyle={{borderColor: ColorDefinitions.primaryText.shade0, borderBottomWidth:1}}
                        inputStyle={{fontFamily: 'alpha-regular'}}
                        label={i18n.t('lbl_email_address')}
                        labelStyle={{fontFamily: 'alpha-regular', fontWeight: TypoDefinitions.primaryText, fontSize: TypoDefinitions.xSmallFont, color: ColorDefinitions.colorAccent}}
                        placeholder='jdoe@gmail.com'
                        onChangeText={val => this._updateInputControl('email', val)}
                    />

                    <TouchableOpacity
                        activeOpacity={.9}
                        onPress={() => {this._updateInputControl('dobPickerVisible', true)}}>
                        <Input
                            editable={false}
                            value={ moment(this.state.dateOfBirth).format('DD.MM.YYYY') }
                            containerStyle={{ paddingTop: 5}}
                            inputContainerStyle={{borderColor: ColorDefinitions.primaryText.shade0, borderBottomWidth:1}}
                            label={i18n.t('lbl_date_of_birth')}
                            labelStyle={{fontFamily: 'alpha-regular', fontWeight: TypoDefinitions.primaryText, fontSize: TypoDefinitions.xSmallFont, color: ColorDefinitions.primaryText.shade0}}
                            inputStyle={{fontFamily: 'alpha-regular', color: 'lightgray'}}
                            placeholder='dd.mm.yyyy'
                        />
                    </TouchableOpacity>

                    <SwitchSelector
                        style={{
                            marginHorizontal: 10,
                            marginVertical: 20,
                            backgroundColor: 'transparent',
                            alignSelf: "center"
                        }}
                        initial={this.props.language === 'en' ? 0 : 1}
                        onPress={(value) => this._updateState('language', value)}
                        textColor={'black'} //'#7a44cf'
                        textStyle={{fontFamily: 'alpha-regular'}}
                        selectedTextStyle={{fontFamily: 'alpha-regular'}}
                        selectedColor={ColorDefinitions.secondaryText.shade0}
                        buttonColor={ColorDefinitions.colorAccent}
                        borderColor={ColorDefinitions.primaryText.shade0}
                        backgroundColor={'rgba(255,255,255,.4)'}
                        hasPadding
                        options={i18n.t('languages')}
                    />
                </View>

            </ScrollView>
        );
    }

    _updateInputControl = (inputKey, inputValue) => {
        this.setState({
            ...this.state,
            [inputKey] : inputValue
        });
    };

    _updateState = (inputKey, inputValue) => {
        this.props.updateGlobalControl({key: inputKey, value: inputValue})
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: ColorDefinitions.mainBackground,
    },

    personalInformation:{

    }
});

const mapStateToProps = (state) => {
    return {
        user: state.authReducer.loggedInUser,
        language: state.globalReducer.language
    }
};

const matchDispatchToProps = dispatch => {
    return {
        updateGlobalControl: (controlObj) => dispatch(updateGlobalControls(controlObj))
    }
};

export default connect(mapStateToProps, matchDispatchToProps)(LandingScreen);
