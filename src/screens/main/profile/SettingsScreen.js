import React, {Component} from 'react';
import {ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import ColorDefinitions from "../../../constants/ColorDefinitions";
import {connect} from "react-redux";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import i18n from "../../../localizations/i18n";
import {Input} from "react-native-elements";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import TypoDefinitions from "../../../constants/TypoDefinitions";
import moment from "moment";
import {Appearance} from "react-native-appearance";

class SettingsScreen extends Component {
    state={
        email: '',
        phone: '',
        givenNames: '',
        surname: '',
        dateOfBirth: new Date(),
        dobPickerVisible: false,
    };

    render () {
        const colorScheme = Appearance.getColorScheme();
        return (
            <ScrollView style={styles.container}>

            </ScrollView>
        );
    }

    _updateInputControl = (inputKey, inputValue) => {
        this.setState({
            ...this.state,
            [inputKey] : inputValue
        });
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: ColorDefinitions.mainBackground,
    },
});

const mapStateToProps = (state) => {
    return {

    }
};

const matchDispatchToProps = dispatch => {
    return {

    }
};

export default connect(mapStateToProps, matchDispatchToProps)(SettingsScreen);
