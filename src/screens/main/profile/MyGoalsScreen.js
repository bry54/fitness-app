import React, {Component} from 'react';
import { ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import ColorDefinitions from "../../../constants/ColorDefinitions";
import {connect} from "react-redux";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import i18n from "../../../localizations/i18n";
import {Input, Button, ListItem, Icon} from "react-native-elements";
import {MaterialCommunityIcons} from "@expo/vector-icons";
import TypoDefinitions from "../../../constants/TypoDefinitions";
import moment from "moment";
import {Appearance} from "react-native-appearance";
import SwitchSelector from "react-native-switch-selector";
import {bmiCalculator, fatRatioCalculator} from '../../../utilities/calculator'

const Indicator = (props) => {
    const item = props.bIndex;
    const value = props.stateValue;
    return(
        <View style={{borderRadius: 10, backgroundColor: 'transparent', padding: 10}}>
            <Text style={{paddingVertical: 5, fontSize: 18, color: ColorDefinitions.primaryText.shade0, fontFamily: 'alpha-regular'}}>{item.title}</Text>
            <Text style={{textAlign:'center', paddingVertical: 5, fontSize: 18, color: 'yellow', fontFamily:  'numeric-regular'}}>{value.num}</Text>
            <Text style={{textAlign:'center', paddingVertical: 5, fontSize: 14, color: 'silver', fontFamily: 'alpha-regular'}}>{value.desc}</Text>
        </View>
    )
}
const Measurement = (props) =>{
    const item = props.measure;
    return (
        <View style={{flex: 0, margin: 10, alignItems: 'center', backgroundColor: ColorDefinitions.primaryBackground.shade0, borderRadius: 10}}>
            <Text style={{paddingVertical: 5, fontSize: 18, color: ColorDefinitions.primaryText.shade0, fontFamily: 'alpha-regular'}}>
                {item.title}
            </Text>
            <Text style={{paddingVertical: 5, fontSize: 20, color: 'yellow', fontFamily: 'numeric-regular'}}>
                {props.stateValue}<Text style={{fontSize: 14}}>{item.unit}</Text>
            </Text>
            <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <Icon
                    raised
                    reverseColor={'silver'}
                    name='plus'
                    type='font-awesome'
                    color={ColorDefinitions.secondaryText.shade0}
                    size={14}
                    onPress={async () => {
                        await props.updateState(item.stateLabel, props.stateValue + 1);
                        await props.updateBodyIndexes()
                    }} />

                <Icon
                    raised
                    name='minus'
                    type='font-awesome'
                    color={ColorDefinitions.secondaryText.shade0}
                    size={14}
                    onPress={async () => {
                        await props.updateState(item.stateLabel, props.stateValue - 1);
                        await props.updateBodyIndexes()
                    }} />
            </View>
        </View>
    )
}

class MyGoalsScreen extends Component {
    state={
        height: 190,
        weight: 78.5,
        targetWeight: 80,
        waist: 78,
        goal: 0,
        bmi: {},
        bodyFat: {},

    };

    componentDidMount() {
        this._updateIndexes()
    }

    render () {
        const measures = [
            {
                title: i18n.t('lbl_height'),
                stateLabel: 'height',
                unit: 'cm'
            },{
                title: i18n.t('lbl_weight'),
                stateLabel: 'weight',
                unit: 'kg'
            },{
                title: i18n.t('lbl_waist'),
                stateLabel: 'waist',
                unit: 'cm'
            }
        ];

        const bodyIndexes = [
            {
                title: i18n.t('lbl_bmi'),
                stateLabel: 'bmi',
            },{
                title: i18n.t('lbl_body_fat'),
                stateLabel: 'bodyFat',
            }
        ]
        return (
            <ScrollView style={styles.container}>
                <View style={{}}>
                    <Text style={{
                        fontSize: 18,
                        fontFamily: 'alpha-regular',
                        backgroundColor: ColorDefinitions.primaryBackground.shade0,
                        padding: 12,
                        color: ColorDefinitions.primaryText.shade0}}>Important Body Indexes</Text>

                    <View style={{flexDirection: 'row', justifyContent: 'space-between', marginVertical: 10}}>
                        {
                            measures.map((measure, index) => (<Measurement
                                key={index}
                                updateState={this._updateInputControl}
                                updateBodyIndexes={this._updateIndexes}
                                stateValue={this.state[measure.stateLabel]}
                                measure={measure}/>))
                        }
                    </View>

                    <View style={{flexDirection: 'row', justifyContent: 'center', backgroundColor: ColorDefinitions.primaryBackground.shade0, marginHorizontal: 10, borderRadius: 10, padding: 10}}>
                        {
                            bodyIndexes.map((bIndex, index) => (<Indicator
                                key={index}
                                stateValue={this.state[bIndex.stateLabel]}
                                bIndex={bIndex}
                            />))
                        }

                    </View>
                </View>

                <View style={{marginVertical: 10 }}>
                    <View>
                        <SwitchSelector
                            style={{marginHorizontal: 10, marginVertical: 20, backgroundColor: 'transparent', alignSelf: "center", borderRadius: 5}}
                            initial={1}
                            onPress={(value) => this._updateInputControl('goal', value)}
                            textColor={'black'} //'#7a44cf'
                            selectedColor={ColorDefinitions.secondaryText.shade0}
                            buttonColor={ColorDefinitions.colorAccent}
                            borderColor={ ColorDefinitions.primaryText.shade0 }
                            backgroundColor={'rgba(255,255,255,.4)'}
                            textStyle={{fontFamily: 'alpha-regular'}}
                            selectedTextStyle={{fontFamily: 'alpha-regular'}}
                            hasPadding
                            options={i18n.t('lbl_goals')}
                        />
                    </View>
                </View>
            </ScrollView>
        );
    }

    _updateInputControl = (inputKey, inputValue) => {
        this.setState({
            ...this.state,
            [inputKey] : inputValue
        });
    };

    _updateIndexes = async () => {
        const weight = this.state.weight;
        const height = this.state.height;
        const waist = this.state.waist;
        const gender = 1

        const bmi = bmiCalculator(weight, height, false)
        await this._updateInputControl('bmi', bmi)

        const bFatRatio = fatRatioCalculator(height, waist, gender)
        await this._updateInputControl('bodyFat', bFatRatio)
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: ColorDefinitions.mainBackground,
    },

    personalInformation:{

    }
});

const mapStateToProps = (state) => {
    return {

    }
};

const matchDispatchToProps = dispatch => {
    return {

    }
};

export default connect(mapStateToProps, matchDispatchToProps)(MyGoalsScreen);
