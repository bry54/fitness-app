import React, {Component} from 'react';
import {ImageBackground, StyleSheet, Text, View} from 'react-native';
import ColorDefinitions from "../constants/ColorDefinitions";
import {connect} from "react-redux";
import {MaterialIndicator} from "react-native-indicators";
import images, {boardingImages} from '../assets/images';
import {Asset} from 'expo-asset';
import {cacheResources} from "../store/utilities/actionsCollection";
import i18n from '../localizations/i18n'
import * as Font from "expo-font";
import {MaterialCommunityIcons, Ionicons} from "@expo/vector-icons";

class SplashScreen extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.navigation.addListener(
            'focus', async () => {
                await this._loadResources();
            }
        );
    }

    render () {
        return (
            <ImageBackground
                fadeDuration={0}
                source={ images.splash} style={styles.container }>
                <View style={styles.splashContainer}>
                    <View>
                        <Text style={styles.headingText}>{i18n.t('app_name')}</Text>
                        <Text style={styles.catchPhraseText}>{i18n.t('catch_phrase')}</Text>
                        <View style={{ marginVertical: 40}}>
                            <MaterialIndicator
                                size={50}
                                animating={true}
                                color={ColorDefinitions.primaryText.shade0}
                                count={7}/>
                        </View>
                        <Text style={styles.waitText}>{i18n.t('string_preparing_app')}</Text>
                    </View>

                    <View style={{paddingVertical: 0}}>
                        <Text style={styles.aboutText}>Fitness App</Text>
                        <Text style={styles.aboutText}>Copyright Protected</Text>
                    </View>
                </View>
            </ImageBackground>
        )
    }

    _mountNextScreen = () => {
        const user = this.props.user;
        const useAsGuest = this.props.useAsGuest;
        const viewedOnBoardingScreen = this.props.viewedOnBoardingScreen;
        const {navigation} = this.props;

        if (!viewedOnBoardingScreen)
            navigation.navigate('OnBoarding');
        else {
            if (user || useAsGuest)
                navigation.navigate('Main');
            else
                navigation.navigate('Auth');
        }
    };

    _cacheImages = (images) => {
        return images.map(image => {
            if (typeof image === 'string') {
                return Image.prefetch(image);
            } else {
                return Asset.fromModule(image).downloadAsync();
            }
        });
    };

    _cacheFonts = (fonts) => {
        return fonts.map(font => Font.loadAsync(font));
    };

    _loadResources = async () =>{
        const imageAssets = this._cacheImages(boardingImages);
        const fontAssets = this._cacheFonts([MaterialCommunityIcons.font, Ionicons.font]);

        const assetsCached = Promise.all([...fontAssets, ...imageAssets]);

        await assetsCached.then( res => {
            this._mountNextScreen();
            this.props.cacheResources();
        }).catch( error =>
            console.log( error )
        );
    }
}

const styles = StyleSheet.create({
    container: {
        height: '100%',
    },

    splashContainer: {
        flex: 1,
        padding: 20,
        marginTop: 100,
        justifyContent: 'space-between',
    },

    headingText: {
        fontSize: 40,
        fontWeight: '800',
        color: ColorDefinitions.primaryText.shade0,
        textAlign: 'center',
        fontFamily: 'alpha-regular'
    },

    catchPhraseText: {
        fontSize: 20,
        fontWeight: '800',
        paddingVertical: 30,
        color: ColorDefinitions.primaryText.shade0,
        textAlign: 'center',
        fontFamily: 'alpha-regular'
    },

    waitText: {
        fontSize: 15,
        paddingVertical: 0,
        color: ColorDefinitions.primaryText.shade0,
        textAlign: 'center',
        fontFamily: 'alpha-regular'
    },

    aboutText:{
        color: ColorDefinitions.primaryText.shade0,
        fontFamily: 'numeric-regular',
        textAlign: 'center',
        paddingVertical: 5
    }
});

const mapStateToProps = (state) => {
    return {
        viewedOnBoardingScreen: state.globalReducer.viewedOnBoardingScreen,
        useAsGuest: state.globalReducer.useAsGuest,
        user: state.authReducer.loggedInUser
    }
};

const matchDispatchToProps = dispatch => {
    return {
        cacheResources: () => dispatch(cacheResources())
    }
};

export default connect(mapStateToProps, matchDispatchToProps)(SplashScreen);
