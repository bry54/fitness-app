import images from "../assets/images";
import React from "react";
import moment from "moment";
import {Text} from "react-native";

export default {
    app_name: 'Elite Kingstar Gym',
    catch_phrase: 'Fitness in your pockets',
    string_preparing_app: 'Preparing app for use, please wait. This can take a while depending on your connection.',
    language_label: 'Select Preferred Language',
    languages: [
        {label: 'English', value: 'en'},
        {label: 'Turkish', value: 'tr'},
    ],
    currency_label: 'Select Display Currency',
    currencies: [
        {label: 'Turkish Lira (TRY)', value: 'TRY'},
        {label: 'Euro (EURO)', value: 'EURO'},
    ],
    boarding_slides: [
        {
            key: '00',
            title: 'Welcome ',
            text: 'You are only a few steps away from getting the body you want.',
            image: images.onboarding._00,
        }, {
            key: '01',
            title: 'More Welcome',
            text: 'More welcome text',
            image: images.onboarding._01,
        }, {
            key: '02',
            title: 'Welcome aboard',
            text: 'Even more welcome text',
            image: images.onboarding._02,
        }, {
            key: '03',
            title: 'Get In',
            text: 'Follow our well created fitness programs.',
            image: images.onboarding._03,
        },
    ],
    skip_boarding: 'Skip',
    title_sign_in: 'Sign In',
    title_sign_up: 'Sign Up',
    title_forgot_password: 'Forgot Password',
    title_skip_registration: 'Skip Registration',
    title_demo_login: 'Demo Login',

    btn_dismiss: 'Dismiss',
    btn_cancel: 'Cancel',
    btn_submit: 'Submit',
    btn_sign_in: 'Sign In',
    btn_sign_up: 'Sign Up',
    btn_forgot_password: 'Forgot Password ?',

    lbl_or: 'or',
    lbl_sign_in_method: 'Sign In Method',
    lbl_membership_number: 'Membership Number',
    lbl_email_address: 'Email Address',
    lbl_personal_information: 'Personal Information',
    lbl_first_name: 'First Name',
    lbl_surname: 'Surname',
    lbl_date_of_birth: 'Date Of Birth',
    lbl_contact_information: 'Contact Information',
    lbl_phone: 'Phone',
    lbl_security_information: 'Security Information',
    lbl_password: 'Password',
    lbl_password_confirm: 'Password Confirm',
    lbl_agree_tc: 'I agree to Elite Kingstar T&C',
    lbl_info_correct: 'I conform all the information is correct',

    menu_home: 'Home',
    title_home: 'Elite Kingstar',
    menu_workouts: 'Workout Programs',
    workouts_subtitle: 'Select your favourite program and get daily recommended workouts.',
    menu_exercises: 'Exercises',
    exercises_subtitle: 'See a well curated list of exercises to stay fit.',
    menu_diets: 'Diets',
    diets_subtitle: 'See our list of recommended healthy meals.',
    menu_motivation: 'Quotes & Motivation',
    motivation_subtitle: 'Find some inspiration to get that body you have always wanted.',
    menu_profile: 'Profile',
    menu_logs: 'Logs',
    menu_settings: 'Settings',
    menu_shop: 'Gym Shop',
    tab_memberships: 'Memberships',
    tab_shop: 'Fitness Products',

    lbl_height: 'Height',
    lbl_weight: 'Weight',
    lbl_target_weight: 'Target Weight',
    lbl_waist: 'Waist',
    lbl_goals: [
        {label: 'Weight Loss', value: '0'},
        {label: 'Maintain', value: '1'},
        {label: 'Muscle Gain', value: '2'}
    ],

    lbl_genders: [
        {label: 'Male', value: '0'},
        {label: 'Female', value: '1'}
    ],

    lbl_bmi: 'Body Mass Index',
    lbl_body_fat: 'Body Fat Percentage',

    lbl_copyright: '@ Copyright '+moment().format('yyyy'),
    lbl_hello: 'Hello',
    lbl_days_remaining: 'Days Remaining',
    lbl_workouts: 'Workouts',
    lbl_meal_description: 'Meal Description',
    lbl_days: 'Day(s)',
    lbl_rest_day: 'Resting  Day',
}
