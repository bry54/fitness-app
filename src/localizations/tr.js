import images from "../assets/images";
import React from "react";
import moment from "moment";

export default {
    app_name: 'Elite Kingstar Gym',
    catch_phrase: 'Cebinizde Fitness',
    string_preparing_app: 'Uygulamayı kullanıma hazırlanıyor, lütfen bekleyin. Bağlantınıza bağlı olarak bu biraz zaman alabilir.',
    language_label: 'Tercih Edilen Dili Seçin',
    languages: [
        {label: 'Ingilizce', value: 'en'},
        {label: 'Türkçe', value: 'tr'},
    ],
    currency_label: 'Görüntü Para Birimini Seçin',
    currencies: [
        {label: 'Türk Lirası (TRY)', value: 'TRY'},
        {label: 'Euro (EURO)', value: 'EURO'},
    ],
    boarding_slides: [
        {
            key: '00',
            title: 'Hoşgeldiniz ',
            text: 'İstediğiniz vücudu almaktan sadece birkaç adım uzaktasınız.',
            image: images.onboarding._00,
        }, {
            key: '01',
            title: 'More Welcome',
            text: 'More welcome text',
            image: images.onboarding._01,
        }, {
            key: '02',
            title: 'Welcome aboard',
            text: 'Even more welcome text',
            image: images.onboarding._02,
        }, {
            key: '03',
            title: 'Get In',
            text: 'İyi oluşturulmuş fitness programlarımızı takip edin.',
            image: images.onboarding._03,
        },
    ],
    skip_boarding: 'Atla',
    title_sign_in: 'Oturum aç',
    title_sign_up: 'Kaydol',
    title_forgot_password: 'Parolanızı mı unuttunuz',
    title_skip_registration: 'Kaydı atla',
    title_demo_login: 'Demo Girişi',

    btn_dismiss: 'Reddet',
    btn_cancel: 'İptal',
    btn_submit: 'Submit',
    btn_sign_in: 'Oturum aç',
    btn_sign_up: 'Kaydol',
    btn_forgot_password: 'Parolanızı mı unuttunuz?',

    lbl_or: 'veya',
    lbl_sign_in_method: 'Oturum Açma Yöntemi',
    lbl_membership_number: 'Üye numarası',
    lbl_email_address: 'Email Adres',
    lbl_personal_information: 'Kişisel bilgi',
    lbl_first_name: 'İsim',
    lbl_surname: 'Soyadı',
    lbl_date_of_birth: 'Doğum tarihi',
    lbl_contact_information: 'İletişim bilgileri',
    lbl_phone: 'Telefon',
    lbl_security_information: 'Güvenlik bilgileri',
    lbl_password: 'Parola',
    lbl_password_confirm: 'Şifre onaylama',
    lbl_agree_tc: 'Elite Kingstar Şart ve Koşullarını kabul ediyorum',
    lbl_info_correct: 'Tüm bilgilerin doğru olduğunu kabul ediyorum',

    menu_home: 'Ev',
    title_home: 'Elite Kingstar',
    menu_workouts: 'Egzersiz Programları',
    menu_exercises: 'Egzersizler',
    exercises_subtitle: 'Formda kalmak için iyi seçilmiş bir egzersiz listesine bakın ...',
    menu_diets: 'Diyetler',
    diets_subtitle: 'Formda kalmak için iyi seçilmiş bir diyet listesine bakın ...',
    menu_motivation: 'Alıntılar ve Motivasyon',
    motivation_subtitle: 'Her zaman istediğin bedeni almak için biraz ilham bul ...',
    menu_profile: 'Profil',
    menu_logs: 'Kütükler',
    menu_settings: 'Ayarlar',

    menu_shop: 'Spor Dükkan',
    tab_memberships: 'Üyelikler',
    tab_shop: 'Spor Ürünleri',

    lbl_height: 'Yükseklik',
    lbl_weight: 'Ağırlık',
    lbl_target_weight: 'Hedef ağırlığı',
    lbl_waist: 'Bel',
    lbl_goals: [
        {label: 'Kilo Kaybı', value: '0'},
        {label: 'Sürdürmek', value: '1'},
        {label: 'Kas Yapmak', value: '2'}
    ],

    lbl_genders: [
        {label: 'Erkek', value: '0'},
        {label: 'Kadın', value: '1'}
    ],

    lbl_bmi: 'Vücut kitle indeksi',
    lbl_body_fat: 'Vücut yağ yüzdesi',

    lbl_copyright: '@ Telif Hakkı '+moment().format('yyyy'),
    lbl_hello: 'Merhaba',
    lbl_days_remaining: 'Kalan Günler',
    lbl_workouts: 'Egzersizler',
    lbl_meal_description: 'Yemek Tanımı',
    lbl_days: 'Gün (ler)',
    lbl_rest_day: 'Dinlenme Günü',
}
