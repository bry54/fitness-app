import i18n from 'i18n-js';
import en from "./en";
import tr from "./tr";

// Set the key-value pairs for the different languages you want to support.
i18n.translations = {
    en: en,
    tr: tr,
};

// Set the locale once at the beginning of your app.
i18n.fallbacks = true;

export default i18n;
