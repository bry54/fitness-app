export default {
    logo: require('./logo.jpg'),
    splash: require('./fitness_00.jpg'),
    meal_preps: require('./meal_preps.jpg'),
    onboarding: {
        _00: require('./fitness_00.jpg'),
        _01: require('./fitness_01.jpg'),
        _02: require('./fitness_03.jpg'),
        _03: require('./fitness_04.jpg'),
        _04: require('./fitness_05.jpg'),
    },

    screens: {
        _00: require('./fitness_00.jpg'),
        _01: require('./fitness_01.jpg'),
        _02: require('./fitness_03.jpg'),
        _03: require('./fitness_04.jpg'),
        _04: require('./fitness_05.jpg'),
    },

    motivation_quotes: {
        story: require('./motivation/story.jpg'),
        quote: require('./motivation/quote.jpg'),
    },

    muscles: {
        abs: {
            m: require('./muscles/m_abs.jpg'),
            f: require('./muscles/f_abs.jpg')
        }, biceps: {
            m: require('./muscles/m_biceps.jpg'),
            f: require('./muscles/f_biceps.jpg')
        }, cardio: {
            m: require('./muscles/m_cardio.jpg'),
            f: require('./muscles/f_cardio.jpg')
        }, chest: {
            m: require('./muscles/m_chest.jpg'),
            f: require('./muscles/f_chest.jpg'),
        }, core: {
            m: require('./muscles/m_core.jpg'),
            f: require('./muscles/f_core.jpg')
        }, legs: {
            m: require('./muscles/m_legs.jpg'),
            f: require('./muscles/f_legs.jpg')
        }, shoulder: {
            m: require('./muscles/m_shoulder.jpg'),
            f: require('./muscles/f_shoulder.jpg')
        }, back: {
            m: require('./muscles/m_back.jpg'),
            f: require('./muscles/f_back.jpg')
        },
    },

    equipments: {

    },

    diets:{
        categories:{
            muscle_gain: {
                f: require('./diets/f_muscle_gain.jpg'),
                m: require('./diets/m_muscle_gain.jpg'),
            }, weight_loss: {
                f: require('./diets/f_weight_loss.jpg'),
                m: require('./diets/m_weight_loss.jpg'),
            }, vegan_meals: {
                f: require('./diets/f_vegan_meal.jpg'),
                m: require('./diets/m_vegan_meal.jpg'),
            }, protein_shakes: {
                f: require('./diets/f_protein_shake.jpg'),
                m: require('./diets/m_protein_shake.jpg'),
            }
        },

        meals:{
            1: 'tet'
        }
    }
}
export const appImages = [

];

export const boardingImages = [
    require('./fitness_00.jpg'),
    require('./fitness_01.jpg'),
    require('./fitness_03.jpg'),
    require('./fitness_04.jpg'),
    require('./fitness_05.jpg'),
];
